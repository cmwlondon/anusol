/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/anusol.js":
/*!********************************!*\
  !*** ./resources/js/anusol.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_InlineCarousel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/InlineCarousel.js */ "./resources/js/components/InlineCarousel.js");
/* harmony import */ var _components_ManualVideo_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/ManualVideo.js */ "./resources/js/components/ManualVideo.js");
/* harmony import */ var _components_QueueSingle_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/QueueSingle.js */ "./resources/js/components/QueueSingle.js");
// require('./bootstrap');
// require('./cookies'); replaced custom cookie consent banner with onetrust cookie settings banner 14/1/2021
__webpack_require__(/*! ./smart-resize */ "./resources/js/smart-resize.js");

__webpack_require__(/*! ./requestAnimFrame */ "./resources/js/requestAnimFrame.js"); // require('./visibilityChange');
// require('./manualvideo');
// require('./InlineCarousel');






function toggleExpander(expander) {
  if (expander.hasClass('open')) {
    expander.removeClass('open');
  } else {
    expander.addClass('open');
  }
}
/*
function getPageVisibilityAPI() {
	let config = {
		"hidden" : null,
		"visibilityChange" : null
	};

	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
	  config.hidden = "hidden";
	  config.visibilityChange = "visibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
	  config.hidden = "msHidden";
	  config.visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
	  config.hidden = "webkitHidden";
	  config.visibilityChange = "webkitvisibilitychange";
	}

	return config;
}

var pageVisibilityAPI;
*/

/* ------------------------------------------------------------------------------------------------ */


$(document).ready(function () {
  // pageVisibilityAPI = getPageVisibilityAPI(); 
  // console.log(pageVisibilityAPI);

  /* ------------------------------------------------------------------------------------- */
  var bannerBreakpoint = 768; // https://developer.mozilla.org/en-US/docs/Web/API/MediaQueryList
  // detect when media queries are applied
  // has the benfit of not needing to continuously monitor window width 

  function screenTest(e) {
    var _mobileMode = e.matches;
    var selector = _mobileMode ? 'mobile' : 'desktop';

    if (_mobileMode !== mobileMode) {
      // change of state
      var _selector = _mobileMode ? 'mobile' : 'desktop';

      mobileMode = _mobileMode; // trigger image switch

      _frames.map(function (_frame) {
        var hdx = _frame.frame.parents('.hdx').eq(0);

        hdx.addClass('loading'); // _frame._delay = window.setTimeout(function(){

        _frame.frame.find('img').on('load', function () {
          hdx.removeClass('loading');
        }).attr({
          "src": _frame[_selector]
        }); // }, 50);

      });
    }
  } // use matchMedia to test if window is wider than 768px or not


  var mql = window.matchMedia('(max-width: 768px)');
  mql.addListener(screenTest);
  var mobileMode = mql.matches;
  var newStateMessage = mobileMode ? 'mobile' : 'desktop';
  var hdx = $('.hdx');
  var frames = $('.hdxframe');
  var _frames = []; // init, runs on page load
  // index frames, select appropriate mode for initial image load

  frames.each(function (i) {
    var frame_img = $(this).find('img');
    var mi = frame_img.attr('data-m');
    var di = frame_img.attr('data-d');

    _frames.push({
      "index": i,
      "frame": $(this),
      "mobile": mi,
      "desktop": di
    });

    var source = mobileMode ? mi : di;
    frame_img.on('load', function (e) {
      hdx.removeClass('loading');
    }).attr({
      "src": source
    });
  });
  /* ------------------------------------------------------------------------------------- */

  /* main navigation menu dropdown */

  /**/

  var menuDelay = null;
  var navMenu = $('.navbar-collapse');
  var navButton = $('.navbar-toggler');
  navButton.on('click', function (e) {
    e.preventDefault();
    navButton.toggleClass('collapsed');

    if (navMenu.hasClass('show')) {
      // close menu
      navMenu.animate({
        "height": "0px"
      }, 500, function (e) {
        navMenu.removeClass('show').css({
          "height": "",
          "overflow": ""
        }); // remove style tag properties
      });
    } else {
      // open menu
      navMenu.css({
        "height": "0",
        "overflow": "hidden"
      }).addClass('show').animate({
        "height": "810px"
      }, 500, function (e) {
        navMenu.css({
          "height": "",
          "overflow": ""
        }); // remove style tag properties
      });
    }
  });
  /**/

  /*
  $('li.nav-item.has-children').on('hover',function(e){
  	$(this).addClass('open-dd');
  });
  */

  /* ------------------------------------------------------------------------------------- */

  /* manual video loader */

  var videos = [];
  $('.manualVideo').each(function () {
    var subjectVideo = new _components_ManualVideo_js__WEBPACK_IMPORTED_MODULE_1__["default"]({
      "module": $(this),
      "loadDelay": 500
    });
    subjectVideo.prepareVideo(); // setTimeout( () => videoHasLoaded($(this)), 500 ); 
  });
  var carouselVideos = ['/video/tv-ad-2kbps.mp4', '/video/vid2-2kbps.mp4', '/video/vid3-2kbps.mp4'];
  /*
  // inline carousel instance unique to home page video carousel
  // allows start of automatic behavour outside of the instance
  let vcItems = null;
  $('#home_vc .inca').each(function(){
  	vcItems = new InlineCarousel({
  		"id" : $(this).attr('data-id'),
  		"slides" : $(this).find('.clmn'),
  		"indicators" : $(this).find('.index > ul > li'),
  		"controls" : $(this).find('.control'),
  		"speed" : $(this).attr('data-speed'),
  		"mode" : $(this).attr('data-mode'),
  		"hasControls" : ($(this).attr('data-has-controls') == 'yes'),
  		"breakpoint" : 920,
  		"pagechanged" : function(){
  			if ( vcControl.runningVideo !== -1) {
  				// reset running video vcControl.runningVideo
  					// stop
  				vcControl.videoTags[vcControl.runningVideo].pause();
  					// show overlay
  				vcControl.videoItems[vcControl.runningVideo].removeClass('running');
  					vcControl.runningVideo = -1;
  			}
  		}
  	});
  });
  // use this to control videoa across entire video carousel
  // stop running video if frame changes
  let vcControl = {
  	"runningVideo" : -1,
  	"videoItems" : [],
  	"videoTags" : []
  }
  */

  if ($('#home_vc2').length > 0) {
    var vcItems = null;
    var _vcControl = {
      "runningVideo": -1,
      "videoItems": [],
      "videoTags": []
    }; // set up slick slider

    $('#home_vc2 .row').slick({
      "autoplay": true,
      "autoplaySpeed": 4000,
      "arrows": true,
      "dots": true,
      "swipe": true,
      "touchMove": true,
      "slidesToShow": 3,
      "slidesToScroll": 3,
      "infinite": true,
      "responsive": [{
        "breakpoint": 960,
        "settings": {
          "slidesToShow": 2,
          "slidesToScroll": 2
        }
      }]
    }); // trigger video overlay when tc advert video item is activated

    $('#home_vc2 .item').each(function (i) {
      $(this).find('a').on('click', function (e) {
        e.preventDefault();
        var videoItem = $(this).parents('.videoPlay').eq(0);
        var video = videoItem.attr('data-video');
        var caption = videoItem.attr('data-caption');
        var poster = videoItem.attr('data-poster');
        $('#videoCaption').text(caption); // insert video tag into popup
        // $('.modal-body')
        // <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video>

        var newVideoTag = $('<video></video>'); // you can't add the 'muted' attribute after the tag has been created

        newVideoTag.attr({
          "preload": "metadata",
          "controls": "",
          "playsinline": "",
          "poster": poster
        }).addClass('waiting'); // attach mp4 source

        var newsrcMP4 = $('<source></source>').attr({
          "type": "video/mp4",
          "src": video
        });
        newVideoTag.append(newsrcMP4);
        $('.modal-body').empty().append(newVideoTag); // trigger video lightbox

        $('body').append($('<div></div>').addClass('modal-backdrop')).addClass('modal-open');
      });
    });
    $('#videopopup button.close').on('click', function (e) {
      e.preventDefault(); // stop video

      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove(); // $('#videopopup').hide();

      $('.modal-body').empty();
    });
    $('.modal-backdrop,.modal').on('click', function (e) {
      e.preventDefault(); // stop video

      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove(); // $('#videopopup').hide();

      $('.modal-body').empty();
    });
    $(".modal-dialog").click(function (e) {
      // Do something
      e.stopPropagation();
    });
  }
  /* sterimar video pop code
          $('.videoPopupTrigger').on('click', function(e){
           e.preventDefault();
            let videoItem = $(this).parents('.videoPlay').eq(0);
           let video = videoItem.attr('data-video');
           let caption = videoItem.attr('data-caption');
           let poster = videoItem.attr('data-poster');
            // insert video tag into popup
           // $('.modal-body')
           // <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video>
           newVideoTag = $('<video></video>');
            // you can't add the 'muted' attribute after the tag has been created
           newVideoTag
           .attr({
             "preload" : "metadata",
             "controls" : "",
             "playsinline" : "",
             "poster" : poster
           })
           .addClass('waiting');
            // attach mp4 source
           let newsrcMP4 = $('<source></source>').attr({
             "type" : "video/mp4",
             "src" : video
           });
           newVideoTag.append(newsrcMP4);
            $('.modal-body')
           .empty()
           .append(newVideoTag);
            $('#videopopup').show();
           $('#videoCaption').text(caption);
           $('body')
           .append( $('<div></div>').addClass('modal-backdrop') )
           .addClass('modal-open');
            
         });
          $('#videopopup button.close').on('click', function(e){
             e.preventDefault();
              // stop video
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
             $('#videopopup').hide();
              $('.modal-body')
             .empty();
         });
          $('.modal-backdrop,.modal').on('click', function(e){
             e.preventDefault();
              // stop video
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
             $('#videopopup').hide();
              $('.modal-body')
             .empty();
         });
          $(".modal-dialog").click(function(e) {
            // Do something
            e.stopPropagation();
         });
          */

  /* */


  var logoQueue = new _components_QueueSingle_js__WEBPACK_IMPORTED_MODULE_2__["default"]({
    "settings": {
      "delay": 50 // quarter second delay between each person fades in

    },
    "actions": {
      "queueStart": function queueStart(queue) {},
      "itemStart": function itemStart(queue, worker) {
        // queue.settings
        // worker
        // <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4="/video/tv-ad-2kbps.mp4"><source type="video/mp4" src="/video/tv-ad-2kbps.mp4"></video>
        var newVideoTag = $('<video></vieo>').attr({
          "preload": "metadata",
          "controls": "",
          "playsinline": "",
          "xloop": ""
        });
        var newVideoSource = $('<source></source>').attr({
          "type": "video/mp4",
          "src": worker.source
        });
        newVideoTag.append(newVideoSource);
        var loadTimeout = window.setTimeout(function () {
          console.log("worker load/error timeout %s", worker.index); // queue.workerComplete();
          // try again

          worker.target.find('video').remove();
          worker.target.append(newVideoTag);
          newVideoTag.on('loadedmetadata', function () {
            console.log("loaded %s", worker.source);
            clearTimeout(loadTimeout); // setTimeout( () => that.videoLoaded($(this)), that.delay ); 

            worker.delay = window.setTimeout(function () {
              vcControl.videoTags[worker.index] = newVideoTag.get(0);
              queue.workerComplete();
            }, queue.settings.delay);
          }).on('error', function (e) {
            console.log("error %s", worker.source);
            clearTimeout(loadTimeout);
            worker.delay = window.setTimeout(function () {
              queue.workerComplete();
            }, queue.settings.delay);
          });
        }, 500);
        newVideoTag.on('loadedmetadata', function () {
          console.log("loaded %s", worker.source);
          clearTimeout(loadTimeout); // setTimeout( () => that.videoLoaded($(this)), that.delay ); 

          worker.delay = window.setTimeout(function () {
            vcControl.videoTags[worker.index] = newVideoTag.get(0);
            queue.workerComplete();
          }, queue.settings.delay);
        }).on('error', function (e) {
          console.log("error %s", worker.source);
          clearTimeout(loadTimeout);
          worker.delay = window.setTimeout(function () {
            queue.workerComplete();
          }, queue.settings.delay);
        });
        console.log("appending %s", worker.source);
        worker.target.append(newVideoTag); // enable play button

        worker.target.find('.videoTrigger3').on('click', function (e) {
          e.preventDefault();
          var thisArticle = $(this).parents('article').eq(0);
          var videoNode = thisArticle.find('video');
          var video = videoNode.get(0);
          video.currentTime = 0;
          video.play();
          vcControl.runningVideo = worker.index;
          thisArticle.addClass('running');
        });
      },
      "itemComplete": function itemComplete(queue, worker) {},
      "queueComplete": function queueComplete(queue) {
        console.log('all videos loaded');
        $('#home_vc').removeClass('loading'); // vcItems.start();

        var highlightedItems = document.querySelectorAll(".overlay");
        highlightedItems.forEach(function (userItem) {
          // userItem.addEventListener('transitionend', () => { console.log('Transition ended'); });
          // userItem.ontransitionend = () => { console.log('Transition ended'); };
          userItem.addEventListener('transitionend', function (e) {
            console.log('Transition ended');
            console.log(e);
          }); // e.propertyName 'top'
          // e.srcElement 'div.overlay'
          // e.target 'div.overlay'
        });
        /*
        // *first* instance of .overlay
        const transition = document.querySelector('#home_vc article div.overlay');
        	transition.addEventListener('transitionend', () => {
          console.log('Transition ended');
        });
        */
      }
    }
  });
  $('#home_vc article').each(function (i) {
    var timeInMs = Date.now(); // compile index of video tags

    vcControl.videoItems.push($(this)); // vcControl.videoTags.push( $(this).find('video').get(0) );

    logoQueue.addItem({
      "index": i,
      "source": $(this).attr('data-video') + '#' + timeInMs,
      "target": $(this)
    });
  });
  logoQueue.startQueue();
  /* */

  /* ------------------------------------------------------------------------------------- */

  /* our-range landing page product carousels */

  var incaArray = [];
  $('.inca').not('#home_vc .inca').each(function () {
    incaArray.push(new _components_InlineCarousel_js__WEBPACK_IMPORTED_MODULE_0__["default"]({
      "id": $(this).attr('data-id'),
      "slides": $(this).find('.clmn'),
      "indicators": $(this).find('.index > ul > li'),
      "controls": $(this).find('.control'),
      "speed": $(this).attr('data-speed'),
      "mode": $(this).attr('data-mode'),
      "hasControls": $(this).attr('data-has-controls') == 'yes',
      "breakpoint": 920
    }));
  });
  /* ------------------------------------------------------------------------------------- */
  // product page details section expander

  $('.expander .header').on('click', function (e) {
    e.preventDefault();
    var expander = $(this).parents('.expander').eq(0);
    toggleExpander(expander);
  });
  /* ------------------------------------------------------------------------------------- */
  // FAQ expander

  $('.expander2 .control').on('click', function (e) {
    e.preventDefault();
    var expander = $(this).parents('.expander2').eq(0);
    toggleExpander(expander);
  });
  /* ------------------------------------------------------------------------------------- */
  // FAQ show more/less

  $('.extraQuestion > .header').on('click', function (e) {
    e.preventDefault();
    var expander = $(this).parents('.extraQuestion').eq(0);
    toggleExpander(expander);
  });
  /* ------------------------------------------------------------------------------------- */
  // bind google analytics click tracking to 'where to buy' retailer links

  $('.retailGrid .cell').on('click', function (e) {
    // e.preventDefault();
    var destination = $(this).attr('href');
    console.log(destination); // trigger google analytics click event

    ga('send', 'event', {
      "eventCategory": 'Outbound Link',
      "eventAction": 'click',
      "eventLabel": destination,
      "transport": 'beacon'
    });
  });
  /* ------------------------------------------------------------------------------------- */
});

/***/ }),

/***/ "./resources/js/components/InlineCarousel.js":
/*!***************************************************!*\
  !*** ./resources/js/components/InlineCarousel.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function InlineCarousel(parameters) {
  this.parameters = parameters;
  this.id = parameters.id;
  this.slides = parameters.slides;
  this.indicators = parameters.indicators;
  this.controls = parameters.controls;
  this.speed = parameters.speed;
  this.mode = parameters.mode;
  this.hasControls = parameters.hasControls;
  this.breakpoint = parameters.breakpoint;
  this.pagechanged = parameters.pagechanged;
  this.index = 0;
  this.oldIndex;
  this.slideCount = 0;
  this.slideAnimation;
  this.animation_is_running = false;
  this.enabled = false;
  this.installed = false;
  this.init();
}

InlineCarousel.prototype = {
  "constructor": InlineCarousel,
  "template": function template() {
    var that = this;
  },
  "init": function init() {
    var that = this;
    this.slideCount = this.slides.length;
    this.module = this.slides.parents('.inca').eq(0);
    this.indexer = this.indicators.parents('.index').eq(0);
    if (this.slideCount > 1) this.enabled = true;

    if (this.enabled) {
      var windowSize = {
        "w": $(window).width()
      };

      if (windowSize.w <= this.breakpoint || this.mode !== 'switch') {
        this.install();
      }

      if (this.mode === 'switch') {
        $(window).smartresize(function () {
          var windowSize = {
            "w": $(window).width()
          };

          if (windowSize.w <= that.breakpoint) {
            that.install();
          } else {
            that.remove();
          }
        }, 10);
      }
    }
  },
  "start": function start() {
    var that = this;
    this.slideAnimation = window.setInterval(function () {
      return that.transition('next');
    }, this.speed); // this.slideAnimation = window.setTimeout(() => that.transition(), this.speed);
    // this.slides.on('transitionend webkitTransitionEnd oTransitionEnd', function (e) {
    // });
    // this.slides.eq(0).get(0).addEventListener('transitionend', function(e){
    // });
    // reset position of slide which has just been moved out of focus			
  },
  "stop": function stop() {
    var that = this; // stop interval

    clearInterval(this.slideAnimation);
    this.slideAnimation = null;
  },
  "transition": function transition(action) {
    var that = this;

    if (this.installed) {
      var slide = -1;
      if (this.pagechanged) this.pagechanged();

      switch (action) {
        case 'next':
          {
            slide = this.nextSlide(this.index);
          }
          break;

        case 'previous':
          {
            slide = this.previousSlide(this.index);
          }
          break;
      }

      this.transitionToSlide(slide, action);
    }
  },
  "transitionToSlide": function transitionToSlide(slide, action) {
    var that = this;
    var currentSlide = this.slides.eq(this.index);
    var nextSlide = this.slides.eq(slide);
    this.oldIndex = this.index;
    this.index = slide; // disable controls while frame transition is running

    this.animation_is_running = true;

    switch (action) {
      case 'next':
        {
          this.transitionR_L(currentSlide, nextSlide);
        }
        break;

      case 'previous':
        {
          this.transitionL_R(currentSlide, nextSlide);
        }
        break;
    } // update indicators


    this.indicators.eq(this.oldIndex).removeClass('active');
    this.indicators.eq(this.index).addClass('active');
  },
  "transitionR_L": function transitionR_L(currentSlide, nextSlide) {
    var that = this; // transition: new frame slides in from right, current frame slide out on left

    currentSlide.css({
      "opacity": "1",
      "left": "0%"
    }).animate({
      "left": "-100%"
    }, 600, function () {
      $(this).css({
        "opacity": "0"
      });
    });
    nextSlide.css({
      "opacity": "1",
      "left": "100%"
    }).animate({
      "left": "0%"
    }, 600, function () {
      that.animation_is_running = false;
    });
  },
  "transitionL_R": function transitionL_R(currentSlide, nextSlide) {
    var that = this; // transition: new frame slides in from left, current frame slide out on right

    currentSlide.css({
      "opacity": "1",
      "left": "0%"
    }).animate({
      "left": "100%"
    }, 600, function () {
      $(this).css({
        "opacity": "0"
      });
    });
    nextSlide.css({
      "opacity": "1",
      "left": "-100%"
    }).animate({
      "left": "0%"
    }, 600, function () {
      that.animation_is_running = false;
    });
  },
  "nextSlide": function nextSlide(slide) {
    var nextSlide = slide + 1;

    if (nextSlide == this.slideCount) {
      nextSlide = 0;
    }

    return nextSlide;
  },
  "previousSlide": function previousSlide(slide) {
    var nextSlide = slide - 1;

    if (nextSlide == -1) {
      nextSlide = this.slideCount - 1;
    }

    return nextSlide;
  },
  "jumpto": function jumpto(slide) {
    // do nothing if the user clicks on the indicator for the current slide
    if (slide !== this.index) {
      if (this.pagechanged) this.pagechanged();

      if (this.mode === 'switch' || this.mode === 'always') {
        // clear interval
        this.stop();
      } // do transition


      this.transitionToSlide(slide, 'next');

      if (this.mode === 'switch' || this.mode === 'always') {
        // start interval
        this.start();
      }
    }
  },
  "docontrol": function docontrol(action) {
    var that = this;

    if (this.mode === 'switch' || this.mode === 'always') {
      // clear interval
      this.stop();
    } // do transition


    this.transition(action);

    if (this.mode === 'switch' || this.mode === 'always') {
      // start interval
      this.start();
    }
  },
  "remove": function remove(slide) {
    var _this = this;

    var that = this;

    if (this.installed) {
      this.stop();
      this.indexer.removeClass('iscontrol');
      this.indicators.off(); // this.module.animate({"opacity" : 0},100,function(){

      this.installed = false;
      window.setTimeout(function () {
        _this.slides.each(function () {
          $(this).css({
            "left": "0%",
            "opacity": "1"
          });
        }); // that.module.animate({"opacity" : 1},100,function(){});

      }, 50); // });
    }
  },
  "install": function install(slide) {
    var that = this;

    if (!this.installed) {
      // this.module.animate({"opacity" : 0},100,function(){
      this.installed = false; // window.setTimeout(() => {

      this.slides.css({
        "left": "100%",
        "opacity": "0"
      });
      this.index = 0;
      this.slides.eq(this.index).css({
        "left": "0%",
        "opacity": "1"
      });
      this.installed = true; // this.module.animate({"opacity" : 1},100,function(){});

      if (this.hasControls) {
        // bind events to indicators
        this.indexer.addClass('iscontrol');
        this.indicators.on('click', function () {
          if (!that.animation_is_running) {
            that.jumpto(that.indicators.index($(this)));
          }
        }); // bind events to previous/next controls

        this.controls.on('click', function () {
          if (!that.animation_is_running) {
            that.docontrol($(this).attr('data-action'));
          }
        });
      }

      if (this.mode === 'switch' || this.mode === 'always') {
        this.start();
      } // },10)
      // });

    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (InlineCarousel);

/***/ }),

/***/ "./resources/js/components/ManualVideo.js":
/*!************************************************!*\
  !*** ./resources/js/components/ManualVideo.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ManualVideo(parameters) {
  this.parameters = parameters;
  this.init();
}

ManualVideo.prototype = {
  "constructor": ManualVideo,
  "template": function template() {
    var that = this;
  },
  "init": function init() {
    var that = this; // console.log('ManualVideo.init()');

    this.module = $(this.parameters.module);
    this.videoTarget = this.module.find('.video').eq(0);
    this.videoSources = {
      "mp4": this.module.attr('data-mp4'),
      "webm": this.module.attr('data-webm'),
      "ogv": this.module.attr('data-ogv')
    };
    this.delay = this.parameters.loadDelay;
  },
  "prepareVideo": function prepareVideo() {
    var that = this; // console.log('ManualVideo.prepareVideo()');

    this.videoTarget.append(this.insertVideo(this.videoSources));
    /* manual video 'Play' button */

    $('.videoTrigger').on('click', function (e) {
      e.preventDefault();
      console.log('play');
      var module = $(this).parents('.manualVideo').eq(0); // remove overlay

      module.addClass('running'); // start video

      var videoItem = module.find('video').eq(0);
      videoItem.get(0).currentTime = 0;
      videoItem.get(0).play();
    });
  },
  "insertVideo": function insertVideo() {
    var _this = this;

    var that = this; // console.log('ManualVideo.insertVideo()');

    this.newVideoTag = $('<video></video>');
    this.newVideoTag.attr({
      "preload": "metadata",
      "controls": "",
      "playsinline": "",
      "xloop": ""
    });
    var types = [["mp4", "mp4"], ["webm", "webm"], ["ogv", "ogg"]];
    types.map(function (type) {
      return _this.buildVideoSource(type);
    }, this); // start video once it is ready to go

    this.newVideoTag.on('loadedmetadata', function () {
      var _this2 = this;

      setTimeout(function () {
        return that.videoLoaded($(_this2));
      }, that.delay);
    });
    /*
    // loop video
    newVideoTag.on('ended',function(){
    	console.log('video ended');
    	$(this).get(0).currentTime = 0;
    	$(this).get(0).play();
    });
    */

    return this.newVideoTag;
  },
  "buildVideoSource": function buildVideoSource(source) {
    var sourceID = source[0];
    var sourceType = source[1];
    var videoSource = this.videoSources[sourceID];

    if (videoSource !== '') {
      var newVideoSourceItem = $('<source></source>').attr({
        "type": "video/" + sourceType,
        "src": videoSource
      });
      this.newVideoTag.append(newVideoSourceItem);
      return sourceID;
    }
  },
  "videoLoaded": function videoLoaded(videoNode) {
    var that = this; // console.log('ManualVideo.videoLoaded()');

    var video = videoNode.get(0),
        readyInfo = video.readyState,
        videoModule = videoNode.parents('.manualVideo').eq(0);
    videoModule.removeClass('loading');
  }
};
/* harmony default export */ __webpack_exports__["default"] = (ManualVideo);

/***/ }),

/***/ "./resources/js/components/QueueSingle.js":
/*!************************************************!*\
  !*** ./resources/js/components/QueueSingle.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function QueueSingle(parameters) {
  this.parameters = parameters;
  this.actions = parameters.actions;
  this.settings = parameters.settings;
  this.workers = [];
  this.workerIndex = 0;
  this.workerCount = 0;
  this.running = false;
  this.init();
}

QueueSingle.prototype = {
  "constructor": QueueSingle,
  "template": function template() {
    var that = this;
  },
  "init": function init() {
    var that = this; // console.log('Queue.init');
    // console.log(this.settings);
  },
  "addItem": function addItem(item) {
    var that = this;
    this.workers.push(item);
    this.workerCount = this.workers.length;
    return this.workerCount;
  },
  "startQueue": function startQueue() {
    var that = this;
    this.actions.queueStart(this);
    this.startWorker();
    this.running = true;
  },
  "startWorker": function startWorker() {
    var that = this;

    if (this.workerIndex < this.workerCount) {
      this.actions.itemStart(this, this.workers[this.workerIndex]);
    } else {
      this.queueComplete();
    }
  },
  "workerComplete": function workerComplete() {
    var that = this;
    this.actions.itemComplete(this, this.workers[this.workerIndex]);
    this.workerIndex++;
    this.startWorker();
  },
  "queueComplete": function queueComplete() {
    var that = this;
    this.actions.queueComplete(this);
    this.running = false;
  }
};
/* harmony default export */ __webpack_exports__["default"] = (QueueSingle);

/***/ }),

/***/ "./resources/js/requestAnimFrame.js":
/*!******************************************!*\
  !*** ./resources/js/requestAnimFrame.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.requestAnimFrame = function (callback) {
  return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
    window.setTimeout(callback, 1000 / 60);
  };
}();

/***/ }),

/***/ "./resources/js/smart-resize.js":
/*!**************************************!*\
  !*** ./resources/js/smart-resize.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
https://www.paulirish.com/2009/throttled-smartresize-jquery-event-handler/
Debounced Resize() jQuery Plugin
August 11th 2009
*/
(function ($, sr) {
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function debounce(func, threshold, execAsap) {
    var timeout;
    return function debounced() {
      var obj = this,
          args = arguments;

      function delayed() {
        if (!execAsap) func.apply(obj, args);
        timeout = null;
      }

      ;
      if (timeout) clearTimeout(timeout);else if (execAsap) func.apply(obj, args);
      timeout = setTimeout(delayed, threshold || 100);
    };
  }; // smartresize 


  jQuery.fn[sr] = function (fn) {
    return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
  };
})(jQuery, 'smartresize'); // usage:


$(window).smartresize(function () {
  // code that takes it easy...
  var windowSize = {
    "w": $(window).width(),
    "h": $(window).height(),
    "a": $(window).width() / $(window).height()
  };
});

/***/ }),

/***/ "./resources/sass/anusol.scss":
/*!************************************!*\
  !*** ./resources/sass/anusol.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*******************************************************************!*\
  !*** multi ./resources/js/anusol.js ./resources/sass/anusol.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/richard.hockey/laravel/anusol/resources/js/anusol.js */"./resources/js/anusol.js");
module.exports = __webpack_require__(/*! /Users/richard.hockey/laravel/anusol/resources/sass/anusol.scss */"./resources/sass/anusol.scss");


/***/ })

/******/ });