<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // https://belanki.com/programming/laravel/a-guide-to-create-custom-404-and-500-page-in-laravel-in-4-steps/
        // 31st October 2019

        if($this->isHttpException($exception))
        {
            switch ($exception->getStatusCode())
            {
                // 404 page not found
                case '404' : {

                    // build context variables for shared page template views/Main/Layouts/main.blade.php
                    $context = [
                        'canonical' => env('APP_URL').'/404',
                        'section' => '',
                        'extra_js' => '',
                        'gaSiteID' => env('GA_SITE_ID'),
                        'meta' => [
                            'title' => '404 Page not found', 
                            'desc' => 'Page not found',
                            'keywords' => 'Page not found',
                            'author' => 'STACK',
                            'og' => [
                                'title' => '404 Page not found',
                                'type' => 'html',
                                'url' => '404',
                                'image' => ''
                            ]
                        ],
                    ];

                    // custom 404 page -> views/errors/404.blade.php
                    return response()->view('errors.404', $context, 404);

                } break;

                /*
                // 500 server error
                case '500' : {
                    echo '500 NotFoundHttpException';
                    return response()->view('errors', [], 500);
                } break;
                */
            }
        } else {
           return parent::render($request, $exception); 
        }
    }
}
