<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class FAQController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section'] = 'faq';
		$this->context['isHomePage'] = false;
	}

	public function index()
	{	



		$this->context['meta']['title'] = 'Piles &amp; Haemorrhoids Frequently Asked Questions – Anusol&trade;';
		$this->context['meta']['desc'] = 'One in two people in the UK suffers from piles at some stage. These are common questions &amp; answers about piles, including what they are and how to treat them.';
		$this->context['meta']['keywords'] = 'Questions, advice, piles, haemorrhoids, common, treatment, answers, FAQ, information, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['isHomePage'] = true;
		$this->context['page'] = 'index';
		
		return view('Main.FAQ.index', $this->context);
	}

	public function helpfulLinks()
	{	
		$this->context['meta']['title'] = 'Piles &amp; Haemorrhoids Help | Anusol&trade; Piles Advice &amp; Treatment';
		$this->context['meta']['desc'] = 'If you need to know more about piles and their treatment, we\'ve put together a list of resources you should find very useful. Find out more here.';
		$this->context['meta']['keywords'] = 'Resources, piles, haemorrhoids, websites, information, advice, useful, help, treatment, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'links';
		
		return view('Main.FAQ.helpful-links', $this->context);
	}

}
