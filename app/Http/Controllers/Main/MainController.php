<?php namespace App\Http\Controllers\Main;

use App;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller {

	public function __construct() {
		$this->uri = Request::path();
		// Context now pulled from config so we can use it in error pages.
		$this->context = [];
		$this->context['meta'] = [];
		$this->context['meta']['title'] = 'ANUSOL';
		$this->context['meta']['desc'] = 'DEFAULT PAGE DESCIPTION';
		$this->context['meta']['keywords'] = 'DEFAULT PAGE KEYWORDS';
		$this->context['meta']['keyphrases'] = 'DEFAULT PAGE KEYPHRASES';
		$this->context['meta']['author'] = 'STACK';
		$this->context['meta']['pagetype'] = 'article';

		// OpenGraph
		// override in page controller
		/*
		<meta property="og:title" content="The Rock" />
		<meta property="og:type" content="video.movie" />
		<meta property="og:url" content="http://www.imdb.com/title/tt0117500/" />
		<meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />
		*/

		$this->domain = env('APP_URL'); // APP_URL in .env, no trailing slash
		$this->context['canonical'] = url()->current();
		$this->context['meta']['og'] = [
			'title' => 'OpenGraph TITLE', // set in page controller
			'description' => 'OpenGraph DESCRIPTION', // set in page controller
			'site_name' => 'ANUSOL&trade',
			'type' => 'website',
			'url' => url()->current(), // get current page URL
			'image' => $this->domain.'/images/furniture/logo_socialshare.jpg',
		];

		$this->context['gaSiteID'] = env('GA_SITE_ID');

		$this->context['extra_js'] = '';
		$this->context['section'] = '';
		$this->titlePrefix = 'ANUSOL';
	}
}
