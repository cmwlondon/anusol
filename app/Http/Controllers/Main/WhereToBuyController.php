<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class WhereToBuyController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section'] = 'where-to-buy';
	}

	public function index()
	{	
		$this->context['meta']['title'] = 'Where to Buy Anusol&trade;';
		$this->context['meta']['desc'] = 'You can buy Anusol&trade; from your nearest pharmacy or major supermarket. Or order it online, to be delivered in plain packaging.';
		$this->context['meta']['keywords'] = 'Buy, delivery, online, suppositories, treatment, piles, haemorrhoids, treatment, discreet, Anusol&trade;';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['isHomePage'] = true;
		$this->context['page'] = 'index';

		$this->context['nodes'] = [
			'asda' => [
				'image' => '/img/where-to-buy/asda.png',
				'title' => 'ASDA',
				'link' => 'https://groceries.asda.com/search/anusol?cmpid=ahc-_-ghs-_-asdacom-_-hp-_-search-anusol'
			],
			'boots' => [
				'image' => '/img/where-to-buy/boots.png',
				'title' => 'Boots',
				'link' => 'https://www.boots.com/sitesearch?searchTerm=anusol'
			],
			'rowlands' => [
				'image' => '/img/where-to-buy/rowlands.png',
				'title' => 'Rowlands Pharmacy',
				'link' => 'https://www.rowlandspharmacy.co.uk/'
			],
			'sainsburys' => [
				'image' => '/img/where-to-buy/sainsburys.png',
				'title' => 'Sainsbury\'s',
				'link' => 'https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10241&storeId=10151&langId=44&krypto=1hrCLa1EYR4IRhRn5ahZTtAH88sibSM65zmLhI%2FrkftT3AmgI6AKRbdwDH8aImGoOnDBIz6KuQsUTk4LQ37GiVfw3qzoI0UXkI%2BSdBoyxRszkDDKlBizCWZP0NjurVvwu531MntaDE7W417pHBmwujW887UU0xZdar%2B2qrUKKAE%3D'
			],
			'lloyds' => [
				'image' => '/img/where-to-buy/lloyds.png',
				'title' => 'Lloyds Pharmacy',
				'link' => 'http://www.lloydspharmacy.com/SearchDisplay?categoryId=&storeId=10151&catalogId=10152&langId=44&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&pageSize=12&searchTerm=anusol#first'
			],
			'superdrug' => [
				'image' => '/img/where-to-buy/superdrug.png',
				'title' => 'Superdrug',
				'link' => 'https://www.superdrug.com/search?text=anusol'
			],
			'tesco' => [
				'image' => '/img/where-to-buy/tesco.png',
				'title' => 'TESCO',
				'link' => 'https://www.tesco.com/groceries/en-GB/search?query=anusol&icid=tescohp_sws-1_m-ft_in-anusol_ab-226-b_out-anusol'
			],
			'amazon' => [
				'image' => '/img/where-to-buy/amazon.png',
				'title' => 'Amazon',
				'link' => 'https://www.amazon.co.uk/s?k=anusol&ref=nb_sb_noss'
			],
			'well' => [
				'image' => '/img/where-to-buy/well.png',
				'title' => 'Well',
				'link' => 'https://www.well.co.uk/'
			]
		];

		return view('Main.WhereToBuy.index', $this->context);
	}

}
