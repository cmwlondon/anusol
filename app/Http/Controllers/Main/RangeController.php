<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class RangeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['section'] = 'our-range';

		$this->titlePrefix = 'ANUSOL - Our Range';
	}

	public function index()
	{	
		$this->context['meta']['title'] = 'Treatment of Piles & Haemorrhoids | Anusol&trade; Piles Advice';
		$this->context['meta']['desc'] = 'There are lots of ways to get better quickly. An effective treatment &amp; a few small changes to your daily routine can really work. Find out more.';
		$this->context['meta']['keywords'] = 'Treatment, piles, haemorrhoids, relief, range, questions, prevention, treatment, fast, effective, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'index';
		
		return view('Main.Range.index', $this->context);
	}

	public function cream()
	{	
 		$this->context['meta']['title'] = 'Anusol&trade; UK Cream Product Page';
		$this->context['meta']['desc'] = 'Pile treatment creams can shrink less severe cases of piles, reducing itching and swelling. They\'re topical antiseptics too. Find out more.';
		$this->context['meta']['keywords'] = 'Cream, treatment, piles, haemorrhoids, soothing, itchy, pain, symptoms, external, internal, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'creams';
		
		return view('Main.Range.cream', $this->context);
	}

	public function ointment()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; UK Ointment Product Page';
		$this->context['meta']['desc'] = 'Like creams, pile treatment ointments can shrink less severe cases of piles, reducing itching and swelling. They\'re topical antiseptics too. Find out more.';
		$this->context['meta']['keywords'] = 'Ointment, treatment, piles, haemorrhoids, soothing, pain, external, internal, lubrication, greasy, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'ointments';

		return view('Main.Range.ointment', $this->context);
	}

	public function suppositories()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; UK Suppositories Product Page';
		$this->context['meta']['desc'] = 'For the treatment of internal piles, suppositories are inserted directly into the back passage, providing medication where it’s needed. Find out more.';
		$this->context['meta']['keywords'] = 'Suppositories, treatment, piles, haemorrhoids, soothing, anus, internal, pain, relief, medication, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'supp';
		
		return view('Main.Range.suppositories', $this->context);
	}

	public function reliefSuppositories()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; Soothing Relief Suppositories';
		$this->context['meta']['desc'] = 'For effective relief from the pain and irritation of internal piles, try Anusol&trade; Soothing Relief Suppositories, with hydrocortisone. Find out more.';
		$this->context['meta']['keywords'] = 'Suppositories, hydrocortisone, treatment, piles, haemorrhoids, soothing, sore, itchy, anus, internal, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'supp-rf';
		
		return view('Main.Range.reliefSuppositories', $this->context);
	}

	public function reliefOintment()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; Soothing Relief Ointment';
		$this->context['meta']['desc'] = 'For powerful relief from the pain and irritation of internal and external piles, try Anusol&trade; Soothing Relief Ointment, with hydrocortisone. Find out more.';
		$this->context['meta']['keywords'] = 'Ointment, lubrication, greasy, treatment, piles, haemorrhoids, soothing, pain, itchy, hydrocortisone, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'ointments-rf';
		
		return view('Main.Range.reliefOintment', $this->context);
	}

	public function hygieneWipes()
	{	
		$this->context['meta']['title'] = 'Hygiene when you suffer from piles | Anusol&trade; &amp trade;';
		$this->context['meta']['desc'] = 'When you have piles, toilet roll can feel a little rough. Anusol&trade; Soothing &amp; Cleansing wipes can help keep you hygienic and comfortable. Find out more.';
		$this->context['meta']['keywords'] = 'Wipes, cleansing, soothing, hygiene, piles, haemorrhoids, sore, itchy, pain, relief, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'wipes';
		
		return view('Main.Range.hygieneWipes', $this->context);
	}

	public function plusHCOintment()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; Plus-HC Ointment';
		$this->context['meta']['desc'] = 'For fast relief from the pain and irritation of more severe internal and external piles, try Anusol&trade; Plus-HC Ointments, with hydrocortisone. Find out more.';
		$this->context['meta']['keywords'] = 'Ointment, lubrication, treatment, piles, hydrocortisone, severe, haemorrhoids, soothing, relief, treatment, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'ointments-ph';
		
		return view('Main.Range.plusHCOintment', $this->context);
	}

	public function plusHCSuppositories()
	{	
		$this->context['meta']['title'] = 'Anusol&trade; Plus-HC Suppositories';
		$this->context['meta']['desc'] = 'For powerful relief from the pain and irritation of more severe internal piles, try Anusol&trade; Plus-HC Suppositories, with hydrocortisone. Find out more.';
		$this->context['meta']['keywords'] = 'Suppositories, treatment, piles, hydrocortisone, severe, haemorrhoids, soothing, pain, relief, treatment, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'supp-ph';
		
		return view('Main.Range.plusHCSuppositories', $this->context);
	}

}
