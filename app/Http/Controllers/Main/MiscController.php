<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use \Route;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->suffix = ' - Anusol&trade; Piles Advice, Symptoms &amp; Treatment';
		$this->context['meta']['keywords'] = 'PAGE KEYWORDS';
		$this->context['section'] = 'misc';

	}

	/*
	public function index()
	{	
		return view('Main.Misc.index', $this->context);
	}
	*/

	public function termsAndConditions()
	{	
		$this->context['meta']['title'] = 'Terms &amp; Conditions'.$this->suffix;
		$this->context['meta']['desc'] = 'By accessing or using this site, you acknowledge that you have read, understood & agreed to the Terms of Use, which you can find in full here.';
		$this->context['meta']['keywords'] = 'Terms, conditions, legal, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'terms';

		return view('Main.Misc.terms_and_conditions', $this->context);
	}

	public function privacy()
	{	
		$this->context['meta']['title'] = 'Privacy policy'.$this->suffix;
		$this->context['meta']['desc'] = 'Church &amp; Dwight UK Limited have established this Internet Privacy Policy which applies to your use of this website &amp; all other Church & Dwight UK websites.';
		$this->context['meta']['keywords'] = 'Privacy, policy, legal';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'privacy';

		return view('Main.Misc.privacy', $this->context);
	}

	public function sitemap()
	{	
		$this->context['meta']['title'] = 'Sitemap'.$this->suffix;
		$this->context['meta']['desc'] = 'website sitemap';
		$this->context['meta']['keywords'] = 'sitemap taxonomy pages navigation structure';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'sitemap';

		return view('Main.Misc.sitemap', $this->context);
	}

	public function xmlSitemap()
	{	
		$siteDomain = request()->root(); // get site domain
		$siteRoutes = app('router')->getRoutes()->get(); // get site routes

		// skip first item in URI list 'api/user';
		array_shift ( $siteRoutes );

		$this->context['pages'] = [];
		foreach ( $siteRoutes AS $route )
		{
			$uri = $route->uri();
			$url = ($uri !== '/') ? $siteDomain."/".$uri : $siteDomain."/";
			$this->context['pages'][] = $url;
		}

		return response()->view('Main.Misc.xml-sitemap', $this->context)->header('Content-Type', 'text/xml');
	}

	public function getInTouch()
	{	
		$this->context['meta']['title'] = 'Get in Touch'.$this->suffix;
		$this->context['meta']['desc'] = 'If you’ve got comments, questions, or general feedback about our products or this website, we’d love to hear from you. Find our contact details here.';
		$this->context['meta']['keywords'] = 'Contact, query, question, call, ask, help, advice, questions, causes, piles, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'contact';


		return view('Main.Misc.get_in_touch', $this->context);
	}

	public function cookies()
	{	
		$this->context['meta']['title'] = 'Cookie Notice'.$this->suffix;
		$this->context['meta']['desc'] = 'Anusol&trade; website cookie usage policy statement';
		$this->context['meta']['keywords'] = 'Cookies, legal, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'cookies';

		return view('Main.Misc.cookies', $this->context);
	}

	public function professionals()
	{	
		$this->context['meta']['title'] = 'Health Care Professionals'.$this->suffix;
		$this->context['meta']['desc'] = 'The therapeutic effectiveness of Anusol&trade; has been demonstrated, time and time again, over more than 85 years of clinical, and home, use.';
		$this->context['meta']['keywords'] = 'Health, care, professionals, piles, haemorrhoids, efficacy, stats, treatment, unique, resource, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'pro';

		return view('Main.Misc.professionals', $this->context);
	}
}
