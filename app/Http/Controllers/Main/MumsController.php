<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class MumsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->titleSuffix = ' | Anusol&trade;';
		$this->context['section'] = 'mums';
	}

	/*
	public function index()
	{	
		return view('Main.Misc.index', $this->context);
	}
	*/

	public function index()
	{	
		$this->context['meta']['title'] = 'Pregnancy Piles Advice – Why Now?'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Piles can be an unpleasant part of pregnancy. But with up to 50% of people suffering at some point in their lives, it’s nothing to be embarrassed about.';
		$this->context['meta']['keywords'] = 'Pregnancy, piles, childbirth, labour, haemorrhoids, sore, itchy, pain, symptoms, advice,  Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'index';

		return view('Main.Mums.index', $this->context);
	}

	/*
	public function treatment()
	{	
		$this->context['meta']['title'] = '';
		$this->context['meta']['desc'] = '';
		$this->context['meta']['keywords'] = ' Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'treatment';

		return view('Main.Mums.treatment', $this->context);
	}
	*/

	public function prevention()
	{	
		$this->context['meta']['title'] = 'Piles in Pregnancy'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Find out how to deal with piles during pregnancy, including lifestyle tips and treatments to ease discomfort.';
		$this->context['meta']['keywords'] = 'Pregnancy, piles, childbirth, haemorrhoids, diet, healthy, fibre, advice, help, mums, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'prevention';

		return view('Main.Mums.prevention', $this->context);
	}

	public function pregnancy()
	{	
		$this->context['meta']['title'] = 'Pregnancy Piles Resources'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'If you\'re looking for a few good resources with advice on women’s health before and after pregnancy, we\'d suggest starting with these helpful links.';
		$this->context['meta']['keywords'] = 'Pregnancy, piles, childbirth, haemorrhoids, pain, symptoms, advice, help, resources, health, Anusol&trade;';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		$this->context['page'] = 'pregnancy';

		return view('Main.Mums.pregnancy', $this->context);
	}
}
