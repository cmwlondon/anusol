<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class AdviceController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->titleSuffix = ' | Anusol&trade;';
		
		$this->context['section'] = 'advice';

		$this->context['meta']['keywords'] = 'PAGE KEYWORDS';
	}

	/*
	public function index()
	{	
		return view('Main.Misc.index', $this->context);
	}
	*/

	public function index()
	{	
		$this->context['meta']['title'] = 'Basic Piles Advice'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Piles are just about the last thing you need. They&rsquo;re a pain – literally – and can have a real impact on your daily routine. Get expert advice here.';
		$this->context['meta']['keywords'] = 'Piles, haemorrhoids, sore, itchy, pain, symptoms, advice, help, causes, questions, Anusol&trade;.';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['page'] = 'index';
		
		return view('Main.Advice.index', $this->context);
	}

	public function causes()
	{	
		$this->context['meta']['title'] = 'Piles Causes'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Get to know the different factors that can increase the risk of piles, from genetics to lifestyle, and how to minimise the risk.';
		$this->context['meta']['keywords'] = 'Causes, piles, haemorrhoids, sore, itchy, pain, symptoms, advice, help, lifestyle, Anusol&trade;';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['page'] = 'causes';

		return view('Main.Advice.causes', $this->context);
	}

	public function symptoms()
	{	
		$this->context['meta']['title'] = 'Piles Symptoms'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Find out how to identify the different symptoms of piles so you know when to visit the doctor and seek treatment and relief.';
		$this->context['meta']['keywords'] = 'Symptoms, piles, haemorrhoids, bleeding, mucous, discomfort, lumpy, sharp, dragging, treatment, Anusol&trade;';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['page'] = 'symptoms';

		return view('Main.Advice.symptoms', $this->context);
	}
	 
	public function treatment()
	{	
		$this->context['meta']['title'] = 'How to Treat Piles'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'Find out how to treat the symptoms of haemorrhoids with the Anusol&trade; range of products, keeping you feeling comfortable through the day.';
		$this->context['meta']['keywords'] = 'Treatment, piles, haemorrhoids, sore, itchy, pain, doctor, online, buy, symptoms, Anusol&trade;';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['page'] = 'treatment';

		return view('Main.Advice.treatment', $this->context);
	}

	public function prevention()
	{	
 		$this->context['meta']['title'] = 'Piles &amp; Haemorrhoids Prevention'.$this->titleSuffix;
		$this->context['meta']['desc'] = 'If you&rsquo;ve had piles, you&rsquo;ll be keen to keep them at bay. And the good news is, a few simple changes can make all the difference. Find out more here.';
		$this->context['meta']['keywords'] = 'Piles, prevention, haemorrhoids, itchy, pain, symptoms, advice, help, healthy, lifestyle, Anusol&trade;';

		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];

		$this->context['page'] = 'prevention';

		return view('Main.Advice.prevention', $this->context);
	}
}
