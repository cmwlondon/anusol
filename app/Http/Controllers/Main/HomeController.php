<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
// use App\Models\Panel;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// public function index(Panel $panels)
	public function index()
	{	
		$this->context['isHomePage'] = true;

		$this->context['meta']['title'] = 'Piles &amp; Haemorrhoids Advice, Symptoms &amp; Treatment - Anusol&trade;';
		$this->context['meta']['desc'] = 'We know piles (haemorrhoids) can be embarrassing, but don\'t suffer in silence. Anusol&trade; can help with symptoms, advice, and effective treatment. Find out more.';
		$this->context['meta']['keywords'] = 'Piles, haemorrhoids, sore, itchy, pain, symptoms, advice, help, relief, questions, Anusol™';
		$this->context['meta']['og']['title'] = $this->context['meta']['title'];
		$this->context['meta']['og']['description'] = $this->context['meta']['desc'];
		
		return view('Main.Home.index', $this->context);
	}

}
