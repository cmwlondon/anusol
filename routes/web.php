<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () { return view('welcome'); });

/* home page */
Route::get('/', 'Main\HomeController@index')->name('home');

/* misc routes */
Route::get('/terms', 'Main\MiscController@termsAndConditions')->name('terms_and_conditions');
Route::get('/privacy', 'Main\MiscController@privacy')->name('privacy');
Route::get('/sitemap', 'Main\MiscController@sitemap')->name('sitemap');
// Route::get('/sitemap.xml', 'Main\MiscController@xmlSitemap')->name('xml_sitemap');
Route::get('/get-in-touch', 'Main\MiscController@getInTouch')->name('get_in_touch');
Route::get('/cookies', 'Main\MiscController@cookies')->name('cookies');
Route::get('/professionals', 'Main\MiscController@professionals')->name('professionals');

/* range */
Route::get('/our-range', 'Main\RangeController@index')->name('our_range');
Route::get('/our-range/cream', 'Main\RangeController@cream')->name('cream');
Route::get('/our-range/ointment', 'Main\RangeController@ointment')->name('ointment');
Route::get('/our-range/suppositories', 'Main\RangeController@suppositories')->name('suppositories');
Route::get('/our-range/relief-suppositories', 'Main\RangeController@reliefSuppositories')->name('relief_suppositories');
Route::get('/our-range/relief-ointment', 'Main\RangeController@reliefOintment')->name('relief_ointment');
Route::get('/our-range/hygiene-wipes', 'Main\RangeController@hygieneWipes')->name('hygiene_wipes');
Route::get('/our-range/plus-hc-ointment', 'Main\RangeController@plusHCOintment')->name('plus_hc_ointment');
Route::get('/our-range/plus-hc-suppositories', 'Main\RangeController@plusHCSuppositories')->name('plus_hc_suppositories');

/* advice */
Route::get('/piles-advice', 'Main\AdviceController@index')->name('piles_advice');
Route::get('/piles-advice/causes', 'Main\AdviceController@causes')->name('causes');
Route::get('/piles-advice/symptoms', 'Main\AdviceController@symptoms')->name('symptoms');
Route::get('/piles-advice/treatment', 'Main\AdviceController@treatment')->name('treatment');
Route::get('/piles-advice/prevention', 'Main\AdviceController@prevention')->name('prevention');

/* just for mums */
Route::get('/just-for-mums', 'Main\MumsController@index')->name('just_for_mums');
// Route::get('/just-for-mums/treatment', 'Main\MumsController@treatment')->name('treatment');
Route::get('/just-for-mums/prevention', 'Main\MumsController@prevention')->name('prevention');
Route::get('/just-for-mums/pregnancy', 'Main\MumsController@pregnancy')->name('pregnancy_resources');

/* where to buy */
Route::get('/where-to-buy', 'Main\WhereToBuyController@index')->name('where_to_buy');

/* FAQs */
Route::get('/top-questions', 'Main\FAQController@index')->name('faq');
Route::get('/top-questions/helpful-links', 'Main\FAQController@helpfulLinks')->name('helpful_links');

/* 301 redirects */
// old URL -> new URL
Route::permanentRedirect('/causes', '/piles-advice/causes');
Route::permanentRedirect('/prevention', '/piles-advice/prevention');
Route::permanentRedirect('/symptoms', '/piles-advice/symptoms');
Route::permanentRedirect('/treatment', '/piles-advice/treatment');
Route::permanentRedirect('/choosing-treatment', '/piles-advice/treatment');
Route::permanentRedirect('/getting-treatment-symptoms', '/piles-advice/treatment');
Route::permanentRedirect('/pregnancy-piles-prevention', '/piles-advice/prevention');
Route::permanentRedirect('/pregancy-piles-treatment', '/piles-advice/treatment');
Route::permanentRedirect('/pregnancy-piles', '/just-for-mums/pregnancy');
Route::permanentRedirect('/piles-treatment', '/piles-advice/treatment');
Route::permanentRedirect('/soothing-wipes', '/our-range/hygiene-wipes');
Route::permanentRedirect('/faq','/top-questions');
Route::permanentRedirect('/just-for-mums-helpful-links', '/top-questions/helpful-links');
Route::permanentRedirect('/piles-advice-helpful-link', '/top-questions/helpful-links');
Route::permanentRedirect('/terms-conditions', '/terms');
Route::permanentRedirect('/privacy-policy', '/privacy');
Route::permanentRedirect('/cookie-notice', '/cookies');
Route::permanentRedirect('/contactus', '/get-in-touch');
Route::permanentRedirect('/SPC', '/professionals');

// Route::permanentRedirect('/', '/'); too many redirects
// Route::permanentRedirect('/piles-advice', '/piles-advice');
// Route::permanentRedirect('/where-to-buy', '/where-to-buy');
// Route::permanentRedirect('/sitemap', '/sitemap');
// Route::permanentRedirect('/get-in-touch', '/get-in-touch');

// Route::permanentRedirect('/page-not-found', '/404');

