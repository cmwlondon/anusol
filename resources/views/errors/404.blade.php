@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

{{-- <picture>

  <source srcset="/img/home/carousel/frame03.jpg" media="(min-width: 768px)">
  <source srcset="/img/home/carousel/frame03m.jpg" media="(max-width: 768px)"> 

  <img src="" alt="" style="width:100%;">

</picture> --}}

<div class="bannery hdx loading" id="fourohfour-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/misc/cookies-banner.jpg" data-m="/img/misc/cookies-banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>404</h1>
	</div>
</div>

<div class="body centred bigmargins">
	<h2>We can't seem to find that page.</h2>
</div>


@endsection

@section('components')
  
@endsection


