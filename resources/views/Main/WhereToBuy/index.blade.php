@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="where-to-buy-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/range/index-page/banner.jpg" data-m="/img/range/index-page/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Where to buy Anusol&trade;</h1>
	</div>
</div>

<div class="body bigmargins  smallBottomMargin centred">
	<p>Anusol&trade; can be bought without prescription, which means an effective treatment for piles is as close as your nearest pharmacy or major supermarket. If you prefer, you can also order Anusol&trade; treatments from the retailers below – which will be delivered straight to your door in plain&nbsp;packaging.</p>	
</div>

{{--
<div class="retailGrid">
	@foreach( $nodes AS $node)
	<a class="cell" target="_blank" title="{{ $node['title'] }}" href="{{ $node['link'] }}"><img alt="{{ $node['title'] }}" src="{{ $node['image'] }}"></a>
	@endforeach
</div>
--}}

<div class="retailGrid2">
	<h2>Supermarkets</h2>
	<div class="grid">
		<div class="cell">
				<div class="box">
					<a href="https://www.tesco.com/groceries/en-GB/search?query=anusol&amp;icid=tescohp_sws-1_m-ft_in-anusol_ab-226-b_out-anusol" title="TESCO" class="logolink" target="_blank"><img alt="TESCO" src="/img/where-to-buy/tesco.png"></a>
					<p>Visit your local Tesco or <a href="https://www.tesco.com/groceries/en-GB/search?query=anusol&amp;icid=tescohp_sws-1_m-ft_in-anusol_ab-226-b_out-anusol" target="_blank">online</a> to find Anusol&trade; Cream and Ointments as well as New Anusol&trade; Soothing &amp; Cleaning Wipes and&nbsp;Suppositories.</p>
				</div>
		</div>
		<div class="cell">
				<div class="box">
					<a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10241&amp;storeId=10151&amp;langId=44&amp;krypto=1hrCLa1EYR4IRhRn5ahZTtAH88sibSM65zmLhI%2FrkftT3AmgI6AKRbdwDH8aImGoOnDBIz6KuQsUTk4LQ37GiVfw3qzoI0UXkI%2BSdBoyxRszkDDKlBizCWZP0NjurVvwu531MntaDE7W417pHBmwujW887UU0xZdar%2B2qrUKKAE%3D" title="Sainsbury&#039;s" class="logolink" target="_blank"><img alt="Sainsbury&#039;s" src="/img/where-to-buy/sainsburys.png"></a>
					<p><a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10241&amp;storeId=10151&amp;langId=44&amp;krypto=1hrCLa1EYR4IRhRn5ahZTtAH88sibSM65zmLhI%2FrkftT3AmgI6AKRbdwDH8aImGoOnDBIz6KuQsUTk4LQ37GiVfw3qzoI0UXkI%2BSdBoyxRszkDDKlBizCWZP0NjurVvwu531MntaDE7W417pHBmwujW887UU0xZdar%2B2qrUKKAE%3D" target="_blank">Shop online</a> or visit your local Sainsbury&rsquo;s supermarket to find Anusol&trade; Cream, Ointments, and Suppositories, as well as flushable New Anusol&trade; Soothing &amp; Cleaning&nbsp;Wipes.</p>
				</div>
		</div>
		<div class="cell">
				<div class="box">
					<a href="https://groceries.morrisons.com/search?entry=anusol" title="Morrisons" class="logolink" target="_blank"><img alt="Morrisons" src="/img/where-to-buy/morrisons.png"></a>
					<p>Buy Anusol&trade; Cream, Soothing &amp; Cleansing Wipes, and Ointments from <a href="https://groceries.morrisons.com/search?entry=anusol" target="_blank">Morrisons online</a> or visit your local store. Most stores also stock Anusol&trade; Suppositories. </p>
				</div>
		</div>
		<div class="cell">
				<div class="box">
					<a href="https://groceries.asda.com/search/anusol?cmpid=ahc-_-ghs-_-asdacom-_-hp-_-search-anusol" title="ASDA" class="logolink" target="_blank"><img alt="ASDA" src="/img/where-to-buy/asda.png"></a>
					<p><a href="https://groceries.asda.com/search/anusol?cmpid=ahc-_-ghs-_-asdacom-_-hp-_-search-anusol" target="_blank">Asda&rsquo;s online groceries store</a> stocks Anusol&trade; Cream, Ointments, and Suppositories, and you can also find our range in your local Asda&nbsp;supermarket.</p>
				</div>
		</div>
	</div>

	<h2>Pharmacies</h2>

	<div class="grid">
		<div class="cell">
			<div class="box">
				<a href="https://www.boots.com/sitesearch?searchTerm=anusol" title="Boots" class="logolink" target="_blank"><img alt="Boots" src="/img/where-to-buy/boots.png"></a>
				<p>Find Anusol&trade; Cream, Ointments, and Suppositories available for delivery from the <a href="https://www.boots.com/sitesearch?searchTerm=anusol" target="_blank">Boots website</a>, or visit your local Boots&nbsp;store.</p>
			</div>
		</div>
		<div class="cell">
			<div class="box">
				<a href="https://www.rowlandspharmacy.co.uk/" title="Rowlands Pharmacy" class="logolink" target="_blank"><img alt="Rowlands Pharmacy" src="/img/where-to-buy/rowlands.png"></a>
				<p>Find your nearest Rowlands Pharmacy to shop the Ansuol range of products, including cream and&nbsp;ointments.</p>
			</div>
		</div>
		<div class="cell">
			<div class="box">
				<a href="https://lloydspharmacy.com/search?q=anusol" title="Lloyds Pharmacy" class="logolink" target="_blank"><img alt="Lloyds Pharmacy" src="/img/where-to-buy/lloyds.png"></a>
				<p>Shop for Anusol&trade; <a href="https://lloydspharmacy.com/search?q=anusol" target="_blank">online at Lloyds Pharmacy</a>, including Anusol&trade; Ointment, Cream, and Suppositories, or visit your local Lloyds&nbsp;store.</p>
			</div>
		</div>
		<div class="cell">
			<div class="box">
				<a href="https://www.superdrug.com/search?text=anusol" title="Superdrug" class="logolink" target="_blank"><img alt="Superdrug" src="/img/where-to-buy/superdrug.png"></a>
				<p>Find the full range of Anusol&trade; products <a href="https://www.superdrug.com/search?text=anusol" target="_blank">online at Superdrug</a>, including Cream, Ointments, Suppositories and New Soothing &amp; Cleansing Wipes, or visit your local&nbsp;Superdrug.</p>
			</div>
		</div>
		<div class="cell">
			<div class="box">
				<a href="https://www.well.co.uk/" title="Well" class="logolink" target="_blank"><img alt="Well" src="/img/where-to-buy/well.png"></a>
				<p>Find your nearest Well pharmarcy to shop the full range of Anusol&trade; Cream, Ointments, and&nbsp;Suppositories.</p>
			</div>
		</div>
	</div>

	<h2>Online</h2>

	<div class="grid">
		<div class="cell">
			<div class="box">
				<a href="https://www.amazon.co.uk/s?k=anusol&amp;ref=nb_sb_noss" title="Amazon" class="logolink" target="_blank"><img alt="Amazon" src="/img/where-to-buy/amazon.png"></a>
				<p>You can buy the full range of Anusol&trade; products on <a href="https://www.amazon.co.uk/s?k=anusol&amp;ref=nb_sb_noss" target="_blank">Amazon</a>, from Cream treatments to Ointments, Suppositories and New Anusol&trade; Soothing &amp; Cleansing&nbsp;Wipes.</p>
			</div>
		</div>
	</div>
</div>

<div class="body left-align bigmargins greyText  smallBottomMargin">
	<h2>Can you get Anusol&trade; over the counter?</h2>
	<p>Yes, you can buy the <a href="/our-range">Anusol&trade; range</a> at any major pharmacy or supermarket without the need for a prescription, providing relief for multiple symptoms of hemorrhoids, including burning and itching. You can choose from Anusol&trade; Cream, Ointments, and Suppositories, as well as flushable New Anusol&trade; Soothing &amp; Cleaning&nbsp;Wipes.</p>

	<a class="btn white view-range left" href="/our-range">View the complete Anusol&trade; range</a>

	<h2>Please note:</h2>
	<p>While the above list provides you with a number of online Anusol&trade; retailers, it is by no means extensive. These sites may contain links or references to other websites to which this Privacy Policy does not apply. We encourage you to read the Privacy Policy of every website you&nbsp;visit.</p>	
</div>

{{-- @include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']]) --}}

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


