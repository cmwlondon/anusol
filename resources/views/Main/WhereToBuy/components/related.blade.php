<div class="threeColumnGrid">
  <div class="row">
  	<div class="clmn our-range">
  		<div>
  			<div class="frame"><img alt="Our Range" src="/img/icons/our-range.png"></div>
  			<h4>Our Range</h4>
  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  			<a href="/our-range" class="btn blue">Our range</a>
  		</div>
  	</div>
  	<div class="clmn faq">
  		<div>
  			<div class="frame"><img alt="Got questions?" src="/img/icons/faq.png"></div>
  			<h4>Got questions?</h4>
  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  			<a href="/top-questions  " class="btn blue">FAQs</a>
  		</div>
  	</div>
  	<div class="clmn get-in-touch">
  		<div>
  			<div class="frame"><img alt="Get in touch" src="/img/icons/get-in-touch.png"></div>
  			<h4>Get in touch</h4>
  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  			<a href="/get-in-touch" class="btn blue">Get in touch</a>
  		</div>
  	</div>
  </div>
</div>

