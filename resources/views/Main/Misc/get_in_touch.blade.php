@extends('Main.Common.main')

@section('header')

@endsection

@section('content')


<div class="bannery hdx loading" id="contact-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/misc/contact-banner.jpg" data-m="/img/misc/contact-banner-mobile.jpg" class="lb">
		</div>
	</div>
	<div class="extra">
		<h1>Get in Touch</h1>
	</div>
</div>

<div class="body left-align bigmargins greyText">

<p>If you&rsquo;ve got comments, questions, or general feedback about our products or this website, we&rsquo;d love to hear from you.</p>

<p>If you have a medical emergency or if you need specific medical advice, you should contact your doctor or your local hospital.</p>

<p>If you would like to speak to someone in person, please contact us on the phone number listed below.</p>

<p>Medical information direct line:<br>
<strong>+44 (0)800 028 1454</strong><br>
Via email: <strong><a href="mailto:aesip@sipdrugsafety.com">aesip@sipdrugsafety.com</a></strong></p>

<p>Customer care direct line:<br>
<strong>+44 (0)800 028 1454</strong><br>
Via email: <strong><a href="mailto:aesip@sipdrugsafety.com">aesip@sipdrugsafety.com</a></strong></p>
 
<p>Sales enquiries only:<br>
<strong>+44 (0)1303 299 592</strong></p>

<p>If you would like to contact us in writing please use the following address.</p>

<address>Ref: ANUSOL&trade;<br>
Church &amp; Dwight UK Limited<br>
Premier House,<br>
Shearway Business Park,<br>
Pent Road,<br>
Folkestone,<br>
Kent,<br>
CT19 4RJ</address>

<p>Email: <strong><a href="mailto:ukenquiries@churchdwight.com">ukenquiries@churchdwight.com</a></strong></p>


</div>

@endsection

@section('components')
  
@endsection


