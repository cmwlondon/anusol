@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="sitemap-banner">
  <div class="image">
    <div class="hdxframe">
      <img alt="" data-d="/img/misc/sitemap-banner.jpg" data-m="/img/misc/sitemap-banner-mobile.jpg" class="rb">
    </div>
  </div>
  <div class="extra">
    <h1>Sitemap</h1>
  </div>
</div>

<div class="body left-align bigmargins">
          <ul class="sitemap">
            <li class="nav-item has-children">
              <a href="/piles-advice" class="nav-link">Piles advice</a>
	            <ul>
	              <li><a href="/piles-advice/causes" class="nav-link">Piles causes</a></li>
	              <li><a href="/piles-advice/symptoms" class="nav-link">Piles symptoms</a></li>
	              <li><a href="/piles-advice/treatment" class="nav-link">Treatment of piles</a></li>
	              <li><a href="/piles-advice/prevention" class="nav-link">Piles prevention</a></li>
	            </ul>
            </li>
            <li class="nav-item has-children">
              <a href="/just-for-mums" class="nav-link">Just for mums</a>
                        <ul>
                          {{-- <li><a href="/just-for-mums/treatment" class="nav-link">Treatment of piles</a></li> --}}
                          <li><a href="/just-for-mums/prevention" class="nav-link">Piles in pregnancy</a></li>
                          <li><a href="/just-for-mums/pregnancy" class="nav-link">Pregnancy resources</a></li>
                        </ul>
            </li>

            <li class="nav-item has-children">
              <a href="/our-range" class="nav-link">Our range</a>
                        <h4>Creams</h4>
                        <ul>
                          <li><a href="/our-range/cream" class="nav-link">Creams</a></li>
                        </ul>
                        <h4>Ointments</h4>
                        <ul>
                          <li><a href="/our-range/ointment" class="nav-link">Ointments</a></li>
                          <li><a href="/our-range/relief-ointment" class="nav-link">Soothing relief ointment</a></li>
                          <li><a href="/our-range/plus-hc-ointment" class="nav-link">Plus-HC ointment</a></li>
                        </ul>
                        <h4>Suppositories</h4>
                        <ul>
                          <li><a href="/our-range/suppositories" class="nav-link">Suppositories</a></li>
                          <li><a href="/our-range/relief-suppositories" class="nav-link">Soothing relief suppositories</a></li>
                          <li><a href="/our-range/plus-hc-suppositories" class="nav-link">Plus-HC suppositories</a></li>
                        </ul>
                        <h4>Wipes</h4>
                        <ul>
                          <li><a href="/our-range/hygiene-wipes" class="nav-link">Hygiene wipes</a></li>
                        </ul>
            </li>

            <li class="nav-item">
              <a href="/where-to-buy" class="nav-link">Where to buy</a>
            </li>
            <li class="nav-item has-children">
              <a href="/top-questions" class="nav-link">Top questions</a>
                        <ul>
                          <li><a href="/top-questions/helpful-links" class="nav-link">Helpful links</a></li>
                        </ul>
            </li>
          </ul>        
</div>

@endsection

@section('components')
  
@endsection


