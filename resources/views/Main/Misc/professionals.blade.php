@extends('Main.Common.main')

@section('header')

@endsection

@section('content')


<div class="bannery hdx loading" id="professionals-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/misc/pro-banner.jpg" data-m="/img/misc/pro-banner-mobile.jpg"  class="rb">
		</div>
	</div>
	<div class="extra">
		<h1>Healthcare Professionals</h1>
	</div>
</div>

<div class="body left-align bigmargins greyText">


<p>The therapeutic effectiveness of Anusol&trade; has been demonstrated, time and time again, over more than 85 years of clinical, and home, use.</p>
<p>And, since it was launched in the UK its current pharmaceutical formulation back in 1932, Anusol&trade; remains a unique product to this day.</p>
 
<p>Anusol&trade; formula contains an astringent to help soothe the area and reduce swelling; an antiseptic to reduce the risk of infection; an emollient to help protect sore skin; and a topical corticosteroid to reduce inflammation, redness and itching.</p>
 
<p>With multiple products formats (including creams, ointments and suppositories) across all pharmaceutical categories (GSL, P, POM) the Anusol&trade; portfolio strategically covers all therapeutic needs, consumer preferences and commercial channels.</p>

<ul class="linkList"> 
	<li><a href="/downloads/products/Anusol%20Soothing%20Relief%20suppositories.pdf" target="_blank">Anusol&trade; Soothing Relief suppositories.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20Soothing%20Relief%20ointment.pdf" target="_blank">Anusol&trade; Soothing Relief ointment.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20plus%20HC%20ointment.pdf" target="_blank">Anusol&trade; Plus-HC ointment.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20ointment.pdf" target="_blank">Anusol&trade; ointment.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20HC%20Suppositories.pdf" target="_blank">Anusol&trade; HC suppositories.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20HC%20ointment.pdf" target="_blank">Anusol&trade; HC ointment.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20cream.pdf" target="_blank">Anusol&trade; cream.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20plus%20HC%20suppositories.pdf" target="_blank">Anusol&trade; Plus-HC suppositories.pdf</a></li>
	<li><a href="/downloads/products/Anusol%20suppositories.pdf" target="_blank">Anusol&trade; suppositories.pdf</a></li>
</div>

@endsection

@section('components')
  
@endsection


