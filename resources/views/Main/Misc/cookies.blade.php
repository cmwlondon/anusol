@extends('Main.Common.main')

@section('header')

@endsection

@section('content')


<div class="bannery hdx loading" id="cookies-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/misc/cookies-banner.jpg" data-m="/img/misc/cookies-banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Cookie Notice</h1>
	</div>
</div>

<div class="body left-align bigmargins greyText">
	<p>Cookies are small text files that are placed on your computer by a website in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. Cookies help us to run the Site and improve its interactivity and our services.</p>
	<p>We have outlined below the types of cookie that are used on this Site.</p>

	<h2>Analytics cookies</h2>
	<p>We use Google Analytics cookies to analyse how this Site is used. Google analytics cookies monitor how visitors navigate the Site and how they found their way here. We use these so that we can see the total amount of visitors to the Site; this does not offer individual information. It also allows us to see what content our users enjoy most which helps us to provide you with the best service. You may at any time change your cookie settings to accept or delete these cookies by using the settings in your web browser.</p>
<!-- OneTrust Cookies List start -->
<div id="ot-sdk-cookie-policy">Cookie Notice</div>
<!-- OneTrust Cookies List end -->
</div>


@endsection

@section('components')
  
@endsection


