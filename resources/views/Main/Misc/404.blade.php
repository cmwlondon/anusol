@extends('Main.Common.main')

@section('header')

@endsection

@section('content')


<div class="bannerx biasRight" id="cookies-banner">
	<h1>Four Oh Four</h1>
	<div class="desktop"><div class="frame"><img alt="" src="/img/misc/cookies-banner.jpg"></div></div>
	<div class="mobile"><div class="frame"><img alt="" src="/img/misc/cookies-banner-mobile.jpg"></div></div>
</div>

<div class="body centred bigmargins">
	<h2>We can't seem to find that page.</h2>
</div>


@endsection

@section('components')
  
@endsection


