<div class="twoColumnGrid alternating">

  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/mums/index/alternating/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Treatment</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/just-for-mums/treatment">The common signs</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="What mums should know" src="/img/mums/index/alternating/pic02.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Prevention</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/just-for-mums/prevention">What you can do</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Finding relief" src="/img/mums/index/alternating/pic03.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Pregnancy resources</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/just-for-mums/pregnancy">Treatment options</a>
	  	</div>
  	</div>
  </div>

</div>
