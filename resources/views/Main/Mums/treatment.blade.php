@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannerx" id="mums-treatment-banner">
	<h1>Treatment of&nbsp;Piles</h1>
	<div class="desktop"><div class="frame"><img alt="" src="/img/mums/treatment/banner.jpg"></div></div>
	<div class="mobile"><div class="frame"><img alt="" src="/img/mums/treatment/banner-mobile.jpg"></div></div>
</div>

<div class="body centred bigmargins">
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<ul class="blueDiscList">
		<li>Lorem ipsum dolor sit amet</li>
		<li>consectetur adipiscing elit</li>
		<li>Nullam malesuada in massa ac tincidunt</li>
		<li>Vivamus laoreet mauris in commodo dapibus</li>
	</ul>
</div>


<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/mums/treatment/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Common causes of piles</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<ul class="blueDiscList">
	  			<li>Lorem ipsum dolor sit amet</li>
	  			<li>consectetur adipiscing elit</li>
	  			<li>Nullam malesuada in massa ac tincidunt</li>
	  			<li>Vivamus laoreet mauris in commodo dapibus</li>
	  		</ul>
	  	</div>
  	</div>
  </div>
</div>

<div class="body centred bigmargins">
	<h2>Piles Symptoms</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum, sapien eu sodales pharetra, massa nisl molestie nibh, quis varius quam nisl ut orci. Praesent eu sapien velit. Curabitur at orci massa. Vestibulum id risus at odio sodales viverra vel nec massa. Etiam eu dignissim lectus, quis suscipit velit. Sed nec ipsum lacinia, tristique dui dapibus, ultrices mi. Aenean nec purus varius, bibendum velit a, sagittis purus. Maecenas</p>	
	<a href="/just-for-mums/prevention" class="btn blue">Next section</a>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


