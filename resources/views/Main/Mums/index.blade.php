@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="mums-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/mums/index/banner.jpg" data-m="/img/mums/index/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Pregnancy piles</h1>
	</div>
</div>

<div class="body intro  centred">
	<p class="lightBlue">Why now?</p>
	<p>As many new mums know, piles often make an unwelcome appearance during pregnancy, or shortly after birth. But, with up to 50% of people suffering at some point in their lives, it&rsquo;s certainly nothing to be embarrassed about.</p>

	<p>So why do piles pick this, already stressful, time to make an appearance? It all comes down to abdominal pressure, which can make your blood vessels stretch and swell. The most obvious culprit: the weight of your ever-growing bump! Then there&rsquo;s all that pushing during labour, too.</p>

	<p>Constipation plays a big part as well. This can be the result of hormonal changes, doing less exercise than normal, changes in your diet, and the weight of your bump pressing on your bowel. These things can all block you up, and straining when you go to the loo is a very common cause of piles.</p>

	<p>In some cases, piles occur with such mild symptoms that you may not even know you have them.</p>
</div>

<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/mums/index/alternating/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Preventing piles</h3>
	  		<p>Once you&rsquo;ve got rid of your piles, the last thing you want is them coming back. Follow our healthy lifestyle tips to give yourself the best chance.</p>
	  		<a class="btn white" href="/just-for-mums/prevention">Prevention</a>
	  	</div>
  	</div>
  </div>
  <div class="row">
  	<div class="clmn p top"><img alt="Want to learn more?" src="/img/mums/index/alternating/pic02.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Want to learn more?</h3>
	  		<p>Check out our handy piles pregnancy resources for even more advice about piles, pregnancy, and healthy lifestyle tips.</p>
	  		<a class="btn white" href="/just-for-mums/pregnancy">Resources</a>
	  	</div>
  	</div>
  </div>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


