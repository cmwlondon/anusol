@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="pregnancy-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/mums/pregnancy/banner.jpg" data-m="/img/mums/pregnancy/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Pregnancy piles resources</h1>
	</div>
</div>

<div class="body left-align bigmargins greyText">
	<p>If you're looking for a few good resources with advice on women's health before and after pregnancy, we'd suggest starting here.</p>
	<h2>Please note:</h2>
	<p>Because these aren't sites we control, we can't be responsible for the content. Use your best judgment.</p>	
</div>

<div class="pregnancyResources">
	<a class="cell" title="TOMMYS" href="https://www.tommys.org/pregnancy-information/labour-birth/after-birth/your-body-after-birth" target="blank" data-link="tommys"><img alt="TOMMYS" src="/img/mums/pregnancy/resources/tommys.png"></a>
	<a class="cell" title="NHS" href="https://www.nhs.uk/conditions/pregnancy-and-baby/piles-haemorrhoids-pregnant/" target="blank" data-link="nhs"><img alt="NHS" src="/img/mums/pregnancy/resources/nhs.png"></a>
	<a class="cell" title="BABYCENTER" href="https://www.babycentre.co.uk/a244/piles-in-pregnancy" target="blank" data-link="babycenter"><img alt="BABYCENTER" src="/img/mums/pregnancy/resources/babycenter.png"></a>
</div>

<div class="body bigmargins greyText centred smallBottomMargin">
	<h2>Have something to share?</h2>
	<p>Is there a helpful, trusted site you think should be on this list? Do let us know about it.</p>	
	<a class="btn blue" href="/get-in-touch">Get in touch</a>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','faq']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


