@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="mums-prevention-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/mums/prevention/banner.jpg" data-m="/img/mums/prevention/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Piles in pregnancy</h1>
	</div>
</div>

<div class="body bigmargins ">

	<p>As a new mum, or mum-to-be, you'll know that piles can be an unexpected (and unpleasant!) result of pregnancy. Caused by too much pressure in the abdomen thanks to your lovely bump, piles during pregnancy are often hard to prevent. But the good news is - in most cases, your symptoms will clear up within a week or two.</p>
	<p>And, by making a few small changes to your lifestyle, you can minimise the chance they'll come back to the same degree, if at all.</p>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Is it normal to have piles during pregnancy?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Piles are something that anyone can get and are very common, in fact 1 in 2 people will experience piles  but some aspects of being pregnant can exacerbate the causes, such as hormones that cause your veins to relax, or because of extra pressure on your abdomen. During pregnancy, they can sometimes happen in the later stages of pregnancy or after you&rsquo;ve given birth.</p> 
				<a class="btn white left wide" href="/piles-advice/causes">Find out more about what causes piles</a>
			</div>
		</div>
	</div>


	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>How can I treat piles during pregnancy?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Piles will likely go away on their own after pregnancy, but you can treat the symptoms of piles to relieve discomfort. Always check with your doctor on what&rsquo;s best to use, whether a Cream, Ointment, or Suppository. These can help to reduce the size of piles as well as soothe inflammation and itching.</p>
				<p>The type of treatment will also depend on whether your piles are internal or external. You may not even notice if you have internal haemorrhoids, but they can pop out during straining, including when giving birth.</p>
				<a class="btn white left wide" href="/piles-advice/treatment">Find out more about treating piles</a>
			</div>
		</div>
	</div>


	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Do pregnancy piles go away?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>After you&rsquo;ve given birth, your piles may reduce and disappear on their own as your body and hormones return to normal, even without treatment. There are also lifestyle changes you can adopt to help get rid of haemorrhoids, or you can use Creams or Ointments like Anusol&trade; to help relieve the symptoms of piles. You&rsquo;ll find tips on both of these below.</p>
				<a class="btn white left wide" href="/piles-advice/prevention">Find out more about preventing piles</a>
			</div>
		</div>
	</div>

	<h2>Some healthy lifestyle tips to help you prevent&nbsp;piles</h2>
	<ul class="blueDiscList">
		<li>Enjoy a diet rich in high-fibre foods, like brown-rice, wholemeal bread, whole-wheat pasta, plenty of fresh fruit, vegetables, and salads.</li>
		<li>Drink plenty of water, as hydration is key to warding off constipation.</li>
		<li>Keep salty, fatty and sugary foods to a minimum, as well as alcohol and caffeinated drinks, which can dehydrate you, leading to straining.</li>
		<li>Take regular gently exercise – like walking or swimming.</li>
		<li>Visit the toilet when you need to, and once you&rsquo;re there not staying too long! Extended periods of time with weight bearing down on your bum is the kind of pressure you want to avoid.</li>
	</ul>



</div>

<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p top"><img alt="Need to know more?" src="/img/mums/prevention/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Need to know more?</h3>
	  		<p>We hope you&rsquo;ve found some helpful answers so far. But if you need to learn more, why not start with these sites?</p>
	  		<a class="btn white" href="/just-for-mums/pregnancy">Helpful Links</a>
	  	</div>
  	</div>
  </div>
</div>

{{-- @include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']]) --}}

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


