
@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

{{--
@include('Main.Home.components.video-carousel')
--}}

<div class="homehero" id="home-hero">
	<div class="items inca" data-speed="5000" data-direction="left" data-animation="slide" data-id="wipes" data-mode="always" data-has-controls="yes">
		<div class="row">
			<div class="clmn active">
				<div class="box">
					<img src="/img/home/carousel/frame03.jpg" class="desktop" alt="Piles are a real pain in the bum">
					<img src="/img/home/carousel/frame03m.jpg" class="mobile" alt="Piles are a real pain in the bum">
				</div>
				<div class="caption">
					<div>
						<h2>Piles are a real pain in the&nbsp;bum</h2>
						<h3>Use Anusol&trade; for soothing relief from&nbsp;piles</h3>
						<a class="btn blue" href="#home-tv-ad">Watch our TV ad now</a>
					</div>
				</div>
			</div>
			{{-- <div class="clmn">
				<div class="box">
					<img src="/img/home/carousel/frame01.jpg" class="desktop" alt="No need to suffer in silence">
					<img src="/img/home/carousel/frame01m.jpg" class="mobile" alt="No need to suffer in silence">
				</div>
				<div class="caption">
					<div>
						<h2>No need to suffer in&nbsp;silence</h2>
						<h3>Use Anusol&trade; for soothing relief from&nbsp;piles</h3>
						<a class="btn blue" href="/our-range">See the range</a>
					</div>
				</div>
			</div> --}}
			<div class="clmn clmn-advice">
				<div class="box">
					<img src="/img/home/carousel/frame02.jpg" class="desktop" alt="Discover our extensive Anusol&trade; range for effective treatment">
					<img src="/img/home/carousel/frame02m.jpg" class="mobile" alt="Discover our extensive Anusol&trade; range for effective treatment">
				</div>
				<div class="caption">
					<div>
						<h2>Discover our extensive Anusol&trade; range for effective&nbsp;treatment</h2>
						<a class="btn blue" href="/our-range">Explore the range</a>
					</div>
				</div>
			</div>
		</div>	
		<div class="control left" data-action="previous">
			<div><img src="/img/icons/previous.png" alt="previous"></div>
		</div>
		<div class="control right" data-action="next">
			<div><img src="/img/icons/next.png" alt="next"></div>
		</div>
		<div class="index">
			<ul>
				<li class="active">1</li>
				<li>2</li>
				{{-- <li>3</li> --}}
			</ul>
		</div>
	</div>
</div>

<div class="body centred intro bigmargins">
	<h2>Piles advice, symptoms &amp; treatment from Anusol&trade;</h2>
	<p>We understand that piles (haemorrhoids) might not be a subject you feel comfortable talking about – but there&rsquo;s no need to suffer in silence!</p> 
	<p>Anusol&trade; is here to help, with expert advice on the symptoms and causes of piles, helpful lifestyle and diet tips to help manage the condition, and easy ways to reduce the likelihood of piles happening in the first place.</p>
	<p>When it comes to finding a widely available and effective treatment, the Anusol&trade; range offers a variety of easy-to-use options – from soothing cream, ointments and suppositories, to handy and hygienic wipes.</p>
	<p>So please, have a look around! We hope you find the help, advice, or product you&rsquo;re searching for.</p>
</div>

<div class="twoColumnGrid alternating">

  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/home/alternating/pic01.jpg" class="lazy"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Not sure it&rsquo;s piles?</h3>
	  		<p>When you&rsquo;re experiencing pain or discomfort when you sit down, or go to the loo, it can be tricky to know what&rsquo;s up! To help you get to the bottom of it, we&rsquo;ve put together a list of the most common symptoms of piles.</p>
	  		<a class="btn blue" href="/piles-advice/symptoms">Check your symptoms</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="What mums should know" src="/img/home/alternating/pic02.jpg" class="lazy"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>What mums should know</h3>
	  		<p>Along with a bouncing bundle of joy, pregnancy and birth can leave you with another new addition&hellip; piles. Find out why you&rsquo;ve got them, what you can do to soothe the area, and even help stop them returning, with our expert tips. </p>
	  		<a class="btn blue" href="/just-for-mums">What you can do</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Finding relief" src="/img/home/alternating/pic03.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Finding soothing relief</h3>
	  		<p>If you do have piles, don&rsquo;t panic! The Anusol&trade; range can help soothe the itch and shrink the size of your piles. Take a look at our complete range of creams, suppositories, or soothing, cooling &amp; cleansing wipes, to decide which one&rsquo;s best for&nbsp;you.</p>
	  		<a class="btn blue" href="/our-range">Explore our range</a>
	  	</div>
  	</div>
  </div>

</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','faq','contact']])

@include('Main.Common.components.video',['id' => 'home-tv-ad', 'mp4' => '/video/tv-ad-2kbps.mp4','webm' => '', 'ogv' => '', 'poster' => '/img/home/video/tv-ad-poster.jpg', 'caption' => 'Watch TV Ad'])

{{--
	<div class="manualVideo" id="{{$id}}">
		<div class="loader"><div></div></div>
		<div class="video">
			<video preload="metadata" controls="" xloop="" playsinline="">
				<source src="{{$mp4}}" type="video/mp4">
			</video>
		</div>
		<div class="overlay">
			<div class="rel">
				<div class="frame"><div class="rel"><img alt="" src="{{$poster}}"></div></div>
				<h2>{{$caption}}</h2>
				<button class="play videoTrigger">PLAY</button>
			</div>
		</div>
	</div>
--}}


@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


