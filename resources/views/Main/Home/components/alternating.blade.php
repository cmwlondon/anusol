<div class="twoColumnGrid alternating">

  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/home/alternating/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Not sure it&rsquo;s piles?</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/piles-advice/causes">The common signs</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="What mums should know" src="/img/home/alternating/pic02.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>What mums should know</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/just-for-mums">What you can do</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Finding relief" src="/img/home/alternating/pic03.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Finding relief</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn blue" href="/piles-advice/treatment">Treatment options</a>
	  	</div>
  	</div>
  </div>

</div>
