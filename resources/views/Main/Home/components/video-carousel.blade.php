{{--
	<section class="vc loading" id="home_vc">
	<div class="items inca" data-speed="5000" data-direction="left" data-animation="slide" data-id="adverts" data-mode="homevc" data-has-controls="yes">
		<div class="row">

			<div class="clmn active">
				<article class="vcItem" data-video="/video/tv-ad-2kbps.mp4">
					<div class="overlay">
						<div class="rel">
							<h2>CAPTION</h2>
							<div class="frame"><div class="rel"><img alt="" src="/img/home/video/tv-ad-poster.jpg"></div></div>
							<button class="play videoTrigger3">PLAY</button>
						</div>
					</div>
					<!-- <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4="/video/tv-ad-2kbps.mp4"><source type="video/mp4" src="/video/tv-ad-2kbps.mp4"></video> -->
				</article>			
			</div>

			<div class="clmn">
				<article class="vcItem" data-video="/video/vid2-2kbps.mp4">
					<div class="overlay">
						<div class="rel">
							<h2>CAPTION</h2>
							<div class="frame"><div class="rel"><img alt="" src="/img/home/video/tv-ad-poster.jpg"></div></div>
							<button class="play videoTrigger3">PLAY</button>
						</div>
					</div>
					<!-- <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4="/video/vid2-2kbps.mp4"><source type="video/mp4" src="/video/vid2-2kbps.mp4"></video> -->
				</article>			
			</div>

			<div class="clmn">
				<article class="vcItem" data-video="/video/vid3-2kbps.mp4">
					<div class="overlay">
						<div class="rel">
							<h2>CAPTION</h2>
							<div class="frame"><div class="rel"><img alt="" src="/img/home/video/tv-ad-poster.jpg"></div></div>
							<button class="play videoTrigger3">PLAY</button>
						</div>
					</div>
					<!-- <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4src="/video/vid3-2kbps.mp4"><source type="video/mp4" src="/video/vid3-2kbps.mp4"></video> -->
				</article>				
			</div>

			<div class="clmn">
				<article class="vcItem" data-video="/video/vid3-2kbps.mp4">
					<div class="overlay">
						<div class="rel">
							<h2>CAPTION</h2>
							<div class="frame"><div class="rel"><img alt="" src="/img/home/video/tv-ad-poster.jpg"></div></div>
							<button class="play videoTrigger3">PLAY</button>
						</div>
					</div>
					<!-- <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4src="/video/vid3-2kbps.mp4"><source type="video/mp4" src="/video/vid3-2kbps.mp4"></video> -->
				</article>				
			</div>

		</div>	
		<div class="control left" data-action="previous">
			<div><img src="/img/icons/previous.png" alt="previous"></div>
		</div>
		<div class="control right" data-action="next">
			<div><img src="/img/icons/next.png" alt="next"></div>
		</div>
		<div class="index">
			<ul>
				<li class="active">1</li>
				<li>2</li>
				<li>3</li>
				<li>3</li>
			</ul>
		</div>
	</div>

	<div class="loader"><div></div></div>
</section>
--}}

<section class="loading" id="home_vc2">
	<div class="row">

		<div class="item">
			<h3><span>ANUSOL</span> ADVERT #1</h3>
			<div class="videoPlay" data-video="/video/tv-ad-2kbps.mp4" data-poster="/img/home/video/poster1.jpg" data-caption="Anusol Advert #1">
				<a href="#" data-trigger="ad01" class="blueplay videoPopupTrigger"></a>
				<img src="/img/home/video/item1.jpg" alt="">
			</div>
		</div>

		<div class="item">
			<h3><span>ANUSOL</span> ADVERT #2</h3>
			<div class="videoPlay" data-video="/video/tv-ad-2kbps.mp4" data-poster="/img/home/video/poster1.jpg" data-caption="Anusol Advert #2">
				<a href="#" data-trigger="ad02" class="blueplay videoPopupTrigger"></a>
				<img src="/img/home/video/item1.jpg" alt="">
			</div>
		</div>
		<div class="item">
			<h3><span>ANUSOL</span> ADVERT #3</h3>
			<div class="videoPlay" data-video="/video/tv-ad-2kbps.mp4" data-poster="/img/home/video/poster1.jpg" data-caption="Anusol Advert #3">
				<a href="#" data-trigger="ad03" class="blueplay videoPopupTrigger"></a>
				<img src="/img/home/video/item1.jpg" alt="">
			</div>
		</div>
		<div class="item">
			<h3><span>ANUSOL</span> ADVERT #4</h3>
			<div class="videoPlay" data-video="/video/tv-ad-2kbps.mp4" data-poster="/img/home/video/poster1.jpg" data-caption="Anusol Advert #4">
				<a href="#" data-trigger="ad04" class="blueplay videoPopupTrigger"></a>
				<img src="/img/home/video/item1.jpg" alt="">
			</div>
		</div>

	</div>
</section>

<div id="videopopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="videoCaption"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>