<div class="manualVideo" id="{{$id}}">
	<div class="loader"><div></div></div>
	<div class="video">
		<video preload="metadata" controls="" xloop="" playsinline="">
			@if ($mp4 !== '' )
			<source src="{{$mp4}}" type="video/mp4">
			@endif
			@if ($webm !== '' )
			<source src="{{$webm}}" type="video/webm">
			@endif
			@if ($ogv !== '' )
			<source src="{{$ogv}}" type="video/ogg">
			@endif
		</video>
	</div>
	<div class="overlay">
		<div class="rel">
			<div class="frame"><div class="rel"><img alt="" src="{{$poster}}"></div></div>
			<h2>{{$caption}}</h2>
			<button class="play videoTrigger">PLAY</button>
		</div>
	</div>
</div>