@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="advice-banner">
  <div class="image">
    <div class="hdxframe">
      <img alt="" data-d="/img/advice/index/banner.jpg" data-m="/img/advice/index/banner-mobile.jpg">
    </div>
  </div>
  <div class="extra">
    <h1>Piles: the basics</h1>
  </div>
</div>

{{--
@include('Main.Advice.components.intro')
@include('Main.Advice.components.alternating')
--}}

<div class="body centred intro bigmargins ">
<p class="lightBlue">Itchy, sore, pains in the bum, piles can really get in the way of your day.</p>
<p>Piles (or &lsquo;haemorrhoids&rsquo;) are the result of increased pressure on the blood vessels in your bottom - causing either internal piles, the most common form, tucked up and away. Or external piles, which protrude from your back passage.</p>
<p>Although it&rsquo;s easy to feel embarrassed, piles are much more common than you think! <strong>One in two</strong> people in the UK will experience them at some point - so, there&rsquo;s no need to suffer in silence.</p>
<p>With an effective treatment in place, you can get back to doing everything you enjoy. So, take a look around, find the information you need, and let&rsquo;s get things sorted!</p>
</div>

<div class="twoColumnGrid alternating">

  <div class="row">
  	<div class="clmn p"><img alt="What causes piles?" src="/img/advice/index/alternating/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>What causes piles?</h3>
	  		<p>There are plenty of confusing myths about piles&hellip; Here&rsquo;s what really causes them.</p>
	  		<a class="btn white" href="/piles-advice/causes">Causes</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/advice/index/alternating/pic02.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Not sure it&rsquo;s piles?</h3>
	  		<p>Check if your symptoms match the ones on our key symptoms list. And, if you&rsquo;re still in any doubt, talk to your doctor.</p>
	  		<a class="btn white" href="/piles-advice/symptoms">Symptoms</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Finding relief" src="/img/advice/index/alternating/pic03.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Finding relief</h3>
	  		<p>Creams, ointments or suppositories, with or without hydrocortisone. As well as soothing &amp; cleansing wipes, whichever you need, or prefer, we have a treatment for you.</p>
	  		<a class="btn white" href="/piles-advice/treatment">Treatment</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
    <div class="clmn p top"><img alt="Preventing piles" src="/img/advice/index/alternating/pic04.jpg"></div>
    <div class="clmn t">
      <div>
        <h3>Preventing piles</h3>
        <p>Although you can&rsquo;t guarantee that piles won&rsquo;t return, you can take these healthy lifestyle steps to make a big difference.</p>
        <a class="btn white" href="/piles-advice/prevention">Prevention</a>
      </div>
    </div>
  </div>

</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


