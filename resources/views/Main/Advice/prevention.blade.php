@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="prevention-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/advice/prevention/banner.jpg" data-m="/img/advice/prevention/banner-mobile.jpg" class="lb">
		</div>
	</div>
	<div class="extra">
		<h1>How to prevent piles</h1>
	</div>
</div>

<div class="body bigmargins ">
	<p>Preventing piles is all about keeping unnecessary pressure off your abdomen. And, though you can&rsquo;t guarantee they won&rsquo;t return, making simple lifestyle changes can really help.</p>
	<h2>Some healthy lifestyle tips to help you prevent piles:</h2>
	<ul class="blueDiscList">
		<li>Enjoy a diet rich in high-fibre foods, like brown-rice, wholemeal bread, whole-wheat pasta, plenty of fresh fruit, vegetables and salads.</li>
		<li>Drink plenty of water, as hydration is key to warding off constipation.</li>
		<li>Keep salty, fatty and sugary foods to a minimum, as well as alcohol and caffeinated drinks, which can dehydrate you, leading to straining.</li>
		<li>Take regular gently exercise – like walking or swimming.</li>
		<li>Visit the loo when you need to, and once you&rsquo;re there not staying too long! Extended periods of time with weight bearing down on your bum is the kind of pressure you want to avoid!</li>
	</ul>
</div>

<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p"><img alt="Need to know more?" src="/img/advice/prevention/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Need to know more?</h3>
	  		<p>We hope you&rsquo;ve found some helpful answers so far. But if you need to learn more, why not start with these sites?</p>
			<a class="btn white" href="/top-questions/helpful-links">Helpful links</a>
	  	</div>
  	</div>
  </div>
</div>

{{-- <div class="body centred bigmargins"></div> --}}

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


