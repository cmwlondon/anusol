@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="symptoms-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/advice/symptoms/banner.jpg" data-m="/img/advice/symptoms/banner-mobile.jpg" class="lb">
		</div>
	</div>
	<div class="extra">
		<h1>Piles symptoms</h1>
	</div>
</div>

<div class="body bigmargins ">
	<p  class="lightBlue centred">The symptoms of piles vary from person to person so if this is the first time you've had piles, it's a good idea to go and see your doctor. While none of the piles symptoms are pleasant, it's important to remember that there's lots you can do to find relief, as well as ways you can reduce the risk of developing piles in the first place.</p>

	<h2>Common piles symptoms:</h2>
	<p>If you think you have piles, look out for these key symptoms:</h2>
	<ul class="blueDiscList">
		<li>Itchiness, soreness and/or inflammation around your anus.</li><li>Bleeding when passing a stool (the blood will be bright red).</li><li>Mucous discharge after passing stool.</li><li>Discomfort or pain while you're going to the loo.</li><li>Feeling like your bowels need to be emptied even after going to the toilet.</li><li>A bulging or lumpy feeling just inside your back passage.</li><li>Hard lump(s) protruding from your bottom.</li><li>Occasional sharp pains in your bottom.</li><li>A dragging sensation in your back passage.</li>
	</ul>
	<p>If you have any other symptoms - especially heavy bleeding from your bottom, or a noticeable change in your stool - see your doctor straight away. These symptoms can also be caused by other ailments from anal fissures to types of cancer, so it is always best to check in with your doctor even if you think it might just be piles.</p>

	<p>To find out more about the likely causes of piles, and who's most affected <a href="/piles-advice/causes">click here</a>.</p>
	<p>And to learn more about piles amongst new mums <a href="/just-for-mums">click here</a>.</p>

	<h2>Common questions about piles symptoms:</h2>
	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>How do I know if I have piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>You may experience one or several of the above symptoms, but still see no external signs of piles. The only way to confirm if you have them is to visit your doctor. This is also important for ruling out any other possible causes of your symptoms. If you experience any of the symptoms listed above, make an appointment with your doctor. </p> 
				{{-- <a class="btn white left wide" href="/piles-advice/causes">Find out more about what causes piles</a> --}}
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>What do piles feel like?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>If you have small, internal piles, you might not be able to feel or see them. Instead, you'll most likely notice one of the above symptoms. If you have larger piles, you may feel a small hard lump on the outside of your anus, or even feel a lump hanging down.</p> 
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Who is most likely to get piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Certain people are more likely like get piles than others, including men and women over 45, pregnant women and new mums, and those carrying extra weight.</p> 
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>When should I go to the doctor about haemorrhoids?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>You should speak to your doctor if you're experiencing any of the symptoms listed above for more than a day, as they could be due to a number of other ailments as well as piles. It's important to get it checked out as soon as possible to make sure it's nothing more serious, and so that you can start treatment to relieve the symptoms as soon as possible.</p>
				<p>Find out more about treating piles here: <a href="/piles-advice/treatment">Treating Piles</a></p> 
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>How do you know if you have internal haemorrhoids?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>If you have small, internal piles, you might not be able to feel or see them. Instead, you'll most likely notice one of the symptoms listed above. If you have larger piles, you may feel a small hard lump on the outside of your anus, or even feel a lump hanging down.</p> 
			</div>
		</div>
	</div>

</div>


<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p height right"><img alt="How can you get relief?" src="/img/advice/symptoms/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>How can you get relief?</h3>
	  		<p>If this is the first time you've had piles, it&rsquo;s a good idea to go and see your doctor. They may recommend an over the counter treatment, which you can buy from your local pharmacy, supermarket and online. And by making little changes to your daily routine, you can start making a difference right&nbsp;away!</p>
			<p>In the meantime, you can find out more about piles treatments below&hellip;</p>
	  		{{-- <ul class="blueDiscList">
	  			<li></li>
	  		</ul>
	  		--}}
	  		<a class="btn white" href="/piles-advice/treatment">Treatment</a>
	  	</div>
  	</div>
  </div>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


