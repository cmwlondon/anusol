@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="treatment-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/advice/treatment/banner.jpg" data-m="/img/advice/treatment/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>How to treat piles</h1>
	</div>
</div>

<div class="body bigmargins smallH2headings">
	<p class="lightBlue centred">While piles can be a pain, don&rsquo;t worry – they can be easily treated</p>

	<p>If it&rsquo;s the first time you&rsquo;ve had piles, be sure to get things checked out with your doctor before self-medicating. They will make a proper diagnosis to put your mind at ease, and recommend the best treatment for your particular symptoms. Don&rsquo;t worry about feeling embarrassed – your doctor will have seen hundreds of similar case and may have even been there themselves!</p>

	<p>If you need a medicine, simply pop into almost any pharmacy or major supermarket. Or buy your treatment online, if you prefer.</p>

	<h2>The Anusol&trade; Range</h2>
	<p>The <a href="/our-range">full Anusol&trade; range</a> provides effective soothing relief for piles. You can choose from Cream, Ointments and Suppositories, each containing an astringent, to help soothe the area and reduce swelling, an antiseptic to reduce the risk of infection, and an emollient to help protect sore skin. Or, our 4-way products, containing hydrocortisone, which reduces inflammation, too.</p>

	<h2 class="product">Anusol&trade; Cream</h2>
	<p>This piles Cream helps to relieve symptoms including itching and swelling, and is ideal for shrinking less severe piles. It&rsquo;s also anti-bacterial, helping to protect against the growth of bacteria.</p>

	<h2 class="product">Anusol&trade; Ointments</h2>
	<p>Our range of Ointments work in a similar way to creams, but with a different consistency. They also provide relief for itching and swelling, helping to reduce inflammation and discomfort. Ointments can also help provide lubrication for suppositories.</p>

	<h2 class="product">Anusol&trade; Suppositories</h2>
	<p>Our range of Suppositories provides relief for piles, helping to shrink them as well as soothe itching and discomfort. They&rsquo;re inserted directly into the anus to provide direct medication, and can be used alongside creams and ointments.</p>

	<h2 class="product">New Anusol&trade; Soothing &amp; Cleansing Wipes</h2>
	<p>Our New Soothing &amp; Cleansing Wipes provide gentle comfort when you have piles, as regular toilet paper can feel harsh and uncomfortable. They&rsquo;re fragrance and alcohol free, hypoallergenic, as well as safely flushable and biodegradable. Gynecologically and dermatologically tested, they are clinically proven to cool, cleanse and soothe the area.</p>

	<a class="btn white left view-range" href="/our-range">View the complete Anusol&trade; range</a>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Which cream is best for piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>You should talk to your doctor to get their recommendation for the best treatment for piles, whether that&rsquo;s Cream, Ointment, or a Suppository. Both creams and ointments are used on the outside of your body to help reduce discomfort from itching and swelling, while a suppository can target internal piles to help reduce their size.</p>
				<p>Normally you&rsquo;ll need to apply cream or ointment several times a day on the outside of your bottom, and practise good hygiene and care when going to the toilet. New Anusol&trade; Soothing & Cleansing Wipes can be a good alternative to dry toilet paper, and ideal for cleaning the area before applying cream or ointment.</p>
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Does Anusol&trade; get rid of&nbsp;piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Anusol&trade; Creams and Ointments help to combat the symptoms of piles and they can sometimes go away on their own, or they made need surgery to remove them. Anusol&trade; helps to relieve itching and discomfort, and are usually only used for a short time as recommended by your doctor.</p>
			</div>
		</div>
	</div>

	<h2>How to stay comfortable with piles&nbsp;symptoms</h2>
	<ul class="blueDiscList">
		<li>Wash your bottom with unperfumed soap after each bowel movement.</li>
		<li>Gently pat the area with a soft dry towel afterwards.</li>
		<li>Wear loose, cotton underwear instead of synthetic fibres, to keep the area as dry as possible.</li>
		<li>Avoid using talcum powder.</li>
	</ul>
	<p>In most cases, your symptoms will clear up within a week or two. And, by making a few small changes to your lifestyle , you can minimise the chance they&rsquo;ll come back to the same degree, if at all.</p>
	<p>With any medication, always make sure you read the label and follow the instructions carefully.</p>
</div>

<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p top"><img alt="Not sure it&rsquo;s piles?" src="/img/advice/treatment/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>How can you prevent&nbsp;piles?</h3>
	  		<p>While there&rsquo;s no way to guarantee they won&rsquo;t return, there&rsquo;s a lot you can do keep them at bay.</p>
	  		<a class="btn white" href="/piles-advice/prevention">Prevention</a>
	  	</div>
  	</div>
  </div>
</div>

{{--
	<div class="body centred bigmargins">
	<h2>Piles Symptoms</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>	
	<a href="/piles-advice/prevention" class="btn blue">Next section</a>
</div>
--}}

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


