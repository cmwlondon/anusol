@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="causes-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/advice/causes/banner.jpg" data-m="/img/advice/causes/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Piles causes</h1>
	</div>
</div>

<div class="body bigmargins ">
	<p class="lightBlue centred">If you've got piles, don't worry, you're not alone. At some point one in two of us will experience piles at some point in their lives, so it's a lot more common issue. 
	<h2>What causes piles?</h2>
	<p>Piles, also known as haemorrhoids, are caused by increased pressure on the blood vessels in your bottom. These blood vessels stretch and swell, which can lead to one or several symptoms, including itchiness, pain, or maybe even bleeding.</p>
	<p>There are many different reasons why you might get piles, from being pregnant to suffering from constipation, and they become more likely as you get older. But anyone can get piles at any time due to all kinds of reasons.</p>

	<h2>Common causes of haemorrhoids:</h2>	
	<ul class="blueDiscList">
		<li><span class="lightBlue">Constipation:</span> Unpleasant enough in itself, the straining that comes with constipation creates pressure on the anus. To soften things up, try upping your intake of fresh fruit and veg. (Find more about piles prevention here).</li>
		<li><span class="lightBlue">Toilet habits:</span> Spending too much time on the toilet, and holding it in when you have to go, can both lead to piles.</li>
		<li><span class="lightBlue">Pregnancy and childbirth:</span> The extra weight of your bundle of joy, plus the constipation that often comes with pregnancy, can put a lot of pressure on your lower organs. Not to mention all that pushing during labour!</li>
		<li><span class="lightBlue">Lifting heavy loads:</span> Whether it's lifting weights at the gym, hoisting your toddler about, or moving boxes at work, picking up heavy loads can sometimes put extra strain on your delicate tissues.</li>
		<li><span class="lightBlue">Being overweight:</span> If you're carrying a lot of extra weight, this could put a strain on your abdomen that leads to piles.</li>
		<li><span class="lightBlue">Family history:</span> You may not know if your parents ever suffered from piles. But if they did, you've got someone else to blame. (At least a little!)</li>
	</ul>

	<h2>Common questions about the causes of piles:</h2>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Can running give you piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Many sports with intense repetitive motions such as running and cycling can put extra strain on the anal area, which can lead to piles. The main way to combat this is to always drink plenty of water when exercising, and maintain a health high-fibre diet.</p> 
				{{-- <a class="btn white left wide" href="/piles-advice/causes">Find out more about what causes piles</a> --}}
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Can stress cause haemorrhoids?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>While not a direct cause of piles, excessive stress can lead to digestive problems such as constipation or diarrhoea, which can increase the risk of haemorrhoid problems.</p> 
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>What foods trigger haemorrhoids?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>While the type of food you eat doesn't directly trigger haemorrhoids, eating a diet low in fibre and not drinking enough water can lead to constipation, which can cause piles due to excessive straining on the loo.</p> 
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Do haemorrhoids run in the family?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>There are lots of different factors that can increase your risk of piles, but genetics can play into it. If you visit the doctor about your piles, they're likely to ask about any family history as a contributing factor, so it's worth knowing this information even though it's not the most pleasant topic to ask your family about!</p> 
			</div>
		</div>
	</div>

</div>

<div class="twoColumnGrid alternating">
  <div class="row">
  	<div class="clmn p top"><img alt="Not sure you&rsquo;ve got them?" src="/img/advice/causes/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Not sure you&rsquo;ve got them?</h3>
	  		<p>If it&rsquo;s your first time, you&rsquo;ll want to see a doctor. But here&rsquo;s a rundown of the common signs.</p>
			<a class="btn white" href="/piles-advice/symptoms">Symptoms of piles</a>
	  	</div>
  	</div>
  </div>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


