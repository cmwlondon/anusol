<div class="threeColumnGrid">
  <div class="row">
  	<div class="clmn where-to-buy">
  		<div>
  			<div class="frame"><img alt="Where to buy" src="/img/icons/where-to-buy.png"></div>
  			<h4>Where to buy?</h4>
  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  			<a href="/where-to-buy" class="btn blue">Where to buy</a>
  		</div>
  	</div>
    <div class="clmn our-range">
      <div>
        <div class="frame"><img alt="Our Range" src="/img/icons/our-range.png"></div>
        <h4>Our Range</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <a href="/our-range" class="btn blue">Our range</a>
      </div>
    </div>
  	<div class="clmn get-in-touch">
  		<div>
  			<div class="frame"><img alt="Get in touch" src="/img/icons/get-in-touch.png"></div>
  			<h4>Get in touch</h4>
  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  			<a href="/get-in-touch" class="btn blue">Get in touch</a>
  		</div>
  	</div>
  </div>
</div>

