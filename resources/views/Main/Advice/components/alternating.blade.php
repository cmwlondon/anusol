<div class="twoColumnGrid alternating">

  <div class="row">
  	<div class="clmn p"><img alt="Not sure it&rsquo;s piles?" src="/img/advice/index/alternating/pic01.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Causes of Piles</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn white" href="/piles-advice/causes">Find out more</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="What mums should know" src="/img/advice/index/alternating/pic02.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Piles Symptoms</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn white" href="/piles-advice/symptoms">Find out more</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
  	<div class="clmn p"><img alt="Finding relief" src="/img/advice/index/alternating/pic03.jpg"></div>
  	<div class="clmn t">
  		<div>
	  		<h3>Treatment of Piles</h3>
	  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
	  		<a class="btn white" href="/piles-advice/treatment">Find out mores</a>
	  	</div>
  	</div>
  </div>

  <div class="row">
    <div class="clmn p"><img alt="Finding relief" src="/img/advice/index/alternating/pic04.jpg"></div>
    <div class="clmn t">
      <div>
        <h3>Piles prevention</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada in massa ac tincidunt. Vivamus laoreet mauris in commodo dapibus. </p>
        <a class="btn white" href="/piles-advice/prevention">Find out more</a>
      </div>
    </div>
  </div>

</div>
