<div id="homeHeaderCarousel" class="carousel slide touch" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>

  <div class="caption">
  	<div>
	  	<h2>No need to suffer in silence</h2>
	  	<h3>Use Anusol&trade; for long lasting <br>relief from piles</h3>
	  	<a class="btn blue" href="/our-range">Explore the range</a>
	</div>
  </div>

  <div class="carousel-inner">
    <div class="carousel-item active home-01">
      <div class="frame"><img src="/img/home/carousel/frame03.jpg" class="d-block w-100" alt="..."></div>
    </div>
    <div class="carousel-item home-02 bg-seventyfive">
      <div class="frame"><img src="/img/home/carousel/frame02.jpg" class="d-block w-100" alt="..."></div>
    </div>
    <div class="carousel-item home-03 bg-right">
      <div class="frame"><img src="/img/home/carousel/frame01.jpg" class="d-block w-100" alt="..."></div>
    </div>
  </div>
  <!--
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
	-->
</div>