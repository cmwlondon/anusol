@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="links-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/range/index-page/banner.jpg" data-m="/img/range/index-page/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Piles help</h1>
	</div>
</div>

<div class="body left-align bigmargins greyText">
	<p>Hopefully, you&rsquo;ve found the information you were looking for, but if you need to know more about piles and their treatment, we&rsquo;ve put together a list of resources you should find very useful.</p>	
	<h2>Please note:</h2>
	<p>Because these aren&rsquo;t sites we control, we can&rsquo;t be responsible for the content. Use your best judgment.</p>
</div>

<div class="retailGrid helpful-links">
	<a class="cell" target="_blank" title="BBC - General information and advice on a range of health issues including information on parenting, travel health, fitness and nutrition. Also features dedicated women's and men's health sections, sexual health advice and the latest BBC health news." href="http://www.bbc.co.uk/health"><img alt="BBC - General information and advice on a range of health issues including information on parenting, travel health, fitness and nutrition. Also features dedicated women's and men's health sections, sexual health advice and the latest BBC health news." src="/img/faqs/helpful-links/logos/bbc.png"></a>
	{{-- <a class="cell" title="beating bowel cancer" href="#[beating-bowel-cancer]"><img alt="beating bowel cancer" src="/img/faqs/helpful-links/logos/beating-bowel-cancer.png"></a> --}}
	<a class="cell" target="_blank" title="netdoctor - An independent UK health site with information on a range of health topics. The site also has an 'Ask the Doctor' section and an interactive discussion group." href="http://www.netdoctor.co.uk"><img alt="netdoctor - An independent UK health site with information on a range of health topics. The site also has an 'Ask the Doctor' section and an interactive discussion group." src="/img/faqs/helpful-links/logos/netdoctor.png"></a>
	<a class="cell" target="_blank" title="NHS - Comprehensive advice from health professionals on a range of health issues. Dedicated sections include a health encyclopaedia, frequently asked questions, healthy living and a guide to the NHS." href="http://www.nhs.uk/conditions/piles-haemorrhoids/"><img alt="NHS - Comprehensive advice from health professionals on a range of health issues. Dedicated sections include a health encyclopaedia, frequently asked questions, healthy living and a guide to the NHS." src="/img/faqs/helpful-links/logos/nhs.png"></a>
</div>

<div class="body centred bigmargins greyText smallBottomMargin">
<h2>Have something to share?</h2>
<p>Is there a helpful, trusted site you think should be on this list? Let us know about it.</p>
<a class="btn white" href="/get-in-touch">Get in touch</a>
</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','faq']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection




