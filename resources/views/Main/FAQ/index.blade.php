@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="faq-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/faqs/index/banner.jpg" data-m="/img/faqs/index/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Top questions</h1>
	</div>
</div>

<div class="body centred bigmargins">
	<p>It&rsquo;s not too bad, <strong>one in two</strong> people in the UK suffers from piles at some stage, but most of us still find it an embarrassing subject to talk&nbsp;about. So we&rsquo;ve put together some common questions and answers about the problem, which should help clear things up.</p>	
</div>

<div class="body faqBlock" style="padding-top:0px !important;">

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>What are piles?</h3>
		</div>
		
		<div class="content">
			<div class="clipper">
				<p>Piles are simply swollen blood vessels in your back passage. They can be internal or external.</p>
			</div>
		</div>
	</div>

	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>What causes them?</h3>
		</div>
		<div class="content">
			<div class="clipper">
				<p>Piles are caused by increased pressure in the blood vessels in your bottom. This can be due to constipation, pregnancy and childbirth, lifting heavy loads and being overweight.</p>
			</div>
		</div>
	</div>
 
	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>Do piles run in the family?</h3>
		</div>
		<div class="content">
			<div class="clipper">
				<p>It hasn't been medically proven, but people do tend to be more susceptible to piles if their parents have them. This could be due to a poor family diet or our physical build.</p>
			</div>
		</div>
	</div>
 
	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>How do I know if it's definitely piles?</h3>
		</div>
		<div class="content">
			<div class="clipper">
				<p>The only way to be 100% sure is to visit your doctor. They'll need to know about your symptoms and may want to do a quick examination.</p>
			</div>
		</div>
	</div>
 
	<div class="expander2">
		<div class="header">
			<div class="control"></div>
			<h3>What's the best treatment to use?</h3>
		</div>
		<div class="content">
			<div class="clipper">
				<p>It depends on your symptoms and your personal preference. There are various treatments available, including creams, ointments and suppositories. The Anusol™ range offers all three and provides effective, soothing relief from discomfort and irritation. Always make sure you read the label & follow instructions carefully.</p>
			</div>
		</div>
	</div>
 

	<div class="extraQuestion">
		<div class="header">
			<h3>Show <span class="open">more</span><span class="close">less</span></h3>
		</div>
		<div class="content">
			<div class="clipper">

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Should I go to my doctor if I think I've got piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>If this is the first time you've developed piles, then yes, it's best to get a proper diagnosis from your doctor. That way you can make sure you're using the most appropriate and effective treatment from the start.</p>
						</div>
					</div>
				</div>
			 
				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Will piles keep coming back?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>It's hard to say, as piles are different for everyone. But there's plenty you can do to try and prevent them, involving a few simple changes to your diet and lifestyle.</p>
						</div>
					</div>
				</div>
			 
				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Can you get piles from sitting on cold floors or hot radiators?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>No! This is a myth. Sitting down for long periods of time can make piles worse, but it doesn't cause them. Developing piles has nothing to do with the temperature of what you're perched on.</p>
						</div>
					</div>
				</div>
			 
				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Are men more susceptible to piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Piles can affect anyone. They're caused by increased pressure in the blood vessels in your bottom and there's no discrimination between the sexes.</p>
						</div>
					</div>
				</div>
			 
				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>I'm overweight – am I at a higher risk for piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Overweight people have a fairly high chance of getting piles due to the extra pressure their weight puts on their abdomen. But this pressure can also be the result of constipation, straining on the loo, pregnancy and lifting heavy weights.</p>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What shrinks haemorrhoids?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>The Anusol™ range offers a variety of product formats for the effective treatment of all types of piles. Applying the right cream, ointment or suppositories, the symptoms can go away in a matter of days or even quicker depending of the severity. </p>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Can piles go away by themselves?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Sometimes they do, and other times you&rsquo;re simply not feeling them anymore, like in the case of internal piles. The important thing is there's action you can take as soon as you feel the first symptoms. Find out more about the benefits of Anusol™.</p>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What is haemorrhoid cream used for?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Anusol™ cream is used to relieve the swelling, itching and irritation of internal or external piles (haemorrhoids).</p>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What cream is best for piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Anusol™ is the number 1 selling  brand* in the UK but there is a variety of products available on the market. Anusol™ contains several active ingredients able to provide soothing relief from piles, for example:</p>
							<ul class="blueDiscList">
								<li>astringents to reduce swelling</li>
								<li>emollients to protect sore skin</li>
								<li>antiseptic to reduce the risk of infection.</li>
							</ul>
							<p>The Anusol™ 4-way formula also contains hydrocortisone which reduces inflammation, 
							swelling, redness and itching.</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>How do you get rid of piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Typically, the recommendations for getting rid of piles involve:</p>
			<ul class="dashList">
				<li>creams to ease the pain, itching and swelling</li><li>treatment to help constipation and soften poo</li><li>use cold packs to ease discomfort</li>
			</ul>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What are the main reasons for piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Piles are swollen blood vessels but things that make piles more likely are:</p>
			<ul class="dashList"><li>constipation</li><li>pushing too hard when pooing</li><li>pregnancy – read about piles during pregnancy</li><li>heavy lifting</li></ul>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Why won't my haemorrhoid go away?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>If there's no improvement to your piles after home treatments, you may need hospital treatment. Talk to your doctor about the best treatment for you. Treatment does not always prevent piles coming back.</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What can you use haemorrhoid cream for?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>If you are experiencing piles, you can use Anusol™ cream for the treatment of haemorrhoids.</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>What is the best haemorrhoid cream?</h3>
					</div>
					<div class="content">
						<div class="clipper">
							<p>Anusol™ is the number 1 selling brand in the UK but there is a variety of products available on the market. Anusol™ contains several active ingredients able to provide soothing relief from piles, for example:</p>

			<ul class="dashList"><li>astringents to reduce swelling</li><li>emollients to protect sore skin</li><li>antiseptic to reduce the risk of infection.</li></ul>

			<p>The Anusol™ 4-way formula also contains hydrocortisone which reduces inflammation, swelling, redness and itching.</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>How do doctors treat haemorrhoids?</h3>
					</div>
					<div class="content">
						<div class="clipper">
							<p>Depending on severity, symptom management can include the following measures:</p>
			<ul class="dashList"><li>Topical haemorrhoid preparations, used morning and night, and after a bowel movement. These include creams, ointments and gels (generally for external haemorrhoids, although some may come with a nozzle for internal application) and suppositories (generally for internal haemorrhoids).</li><li>Simple painkillers</li><li>Warm baths or ice packs</li><li>Non-surgical treatments (such as rubber band ligation, electrotherapy or sclerotherapy)</li><li>Surgical treatments</li></ul>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Can you push a haemorrhoid back in?</h3>
					</div>
					<div class="content">
						<div class="clipper">
							<p>Yes, you can gently push a pile back inside.</p>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>How can I permanently get rid of piles?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>While there&rsquo;s no guaranteed way to permanently get rid of piles, along with the right treatment, it can help to make some positive lifestyle changes too. Here are just a few: include more fibre-rich food in your diet, drink plenty of water, cut down on anything that causes constipation - like alcohol and caffeine, exercise regularly, and try not to spend too much time on the loo!</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>Can you buy piles treatment online?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Yes, you can buy piles treatment online. The Anusol™ range is widely available online and in store. See <a href="/where-to-buy">here</a> the most suitable place for you.</p>

						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>How can I treat piles at home?</h3>
					</div>
					<div class="content">
						
						<div class="clipper">
							<p>Choose the most suitable Anusol™ product for your needs and treat piles at home. During a haemorrhoid episode, you can also:</p>
							<ul class="dashList">
								<li>drink lots of fluid and eat plenty of fibre to keep your poo soft</li>
								<li>wipe your bottom with damp toilet paper</li>
								<li>take paracetamol if piles hurt</li>
								<li>take a warm bath to ease itching and pain</li>
								<li>use an ice pack wrapped in a towel to ease discomfort</li>
								<li>gently push a pile back inside</li>
								<li>keep your bottom clean and dry</li>
								<li>exercise regularly</li>
								<li>cut down on alcohol and caffeine (like tea, coffee and cola) to avoid constipation</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="expander2">
					<div class="header">
						<div class="control"></div>
						<h3>How do I get rid of piles naturally and fast?</h3>
					</div>
					<div class="content">
						<div class="clipper">
			<p>There is a variety of lifestyle factors that can have an impact on the incidence of haemorrhoids which you can adjust to prevent re-occurrence of piles:</p>
			<ul class="dashList"><li>Constipation</li><li>Chronic diarrhoea</li><li>Straining while trying to pass stools; prolonged time sitting on the toilet</li><li>Low-fibre diet</li><li>Obesity</li><li>Regular heavy lifting</li><li>Ageing</li><li>Cirrhosis with ascites (accumulation of fluid in the abdominal cavity)</li><li>Chronic cough</li></ul>
						</div>
					</div>
				</div>

				{{-- <a href="/top-questions" class="btn darkblue">More FAQs</a> --}}
			</div>

			{{--
				<div class="twoColumnGrid alternating">
			  <div class="row">
			  	<div class="clmn p"><img alt="Wondering where to buy?" src="/img/advice/causes/pic01.jpg"></div>
			  	<div class="clmn t">
			  		<div>
				  		<h3>Wondering where to buy?</h3>
				  		<p>Effective relief is as near as your closest pharmacy. But you can also buy Anusol™ online.</p>
				  		<a href="/where-to-buy" class="btn blue">Where to buy</a>
				  	</div>
			  	</div>
			  </div>
			</div>
			--}}

			</div>
		</div>


</div>

@include('Main.Common.components.related',['links' => ['where-to-buy','range','contact']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


