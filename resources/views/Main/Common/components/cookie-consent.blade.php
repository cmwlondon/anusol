        <div class="cookie-notice">
            <div class="ch">
                <div class="cl">
                    <p>This site uses cookies as described in our <a href="/cookies">cookie policy</a>. <br>Please continue to use the site if you agree to our use of cookies.</p>
                </div>
                <div class="cr">
                    <button class="btn blue cookie-consent-accept">Continue</button>
{{--
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings cookie-consent-accept">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->
--}} 
                </div>
            </div>
        </div>
