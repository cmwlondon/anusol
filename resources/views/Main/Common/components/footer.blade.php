<footer class="page">
	<a href="/" class="logo" title="Anusol&trade;"><img alt="Anusol&trade;" src="/img/logo/anusol_footer.png"></a>
	<nav>
		<a href="/terms" class="@if (($section === 'misc') && ($page === 'terms') )active @endif">Terms&nbsp;and&nbsp;Conditions</a>
		<a href="/privacy" class="@if (($section === 'misc') && ($page === 'privacy') )active @endif">Privacy&nbsp;Policy</a>
		<a href="/cookies" class="@if (($section === 'misc') && ($page === 'cookies') )active @endif">Cookies</a>
		<a href="/sitemap" class="@if (($section === 'misc') && ($page === 'sitemap') )active @endif">Sitemap</a>
		<a href="/get-in-touch" class="@if (($section === 'misc') && ($page === 'contact') )active @endif">Get&nbsp;In&nbsp;Touch</a>
		<a href="/professionals" class="@if (($section === 'misc') && ($page === 'pro') )active @endif">Healthcare&nbsp;Professionals</a>
	</nav>
	<div>

<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->

{{--
	<!-- OneTrust Cookies List start -->
<div id="ot-sdk-cookie-policy">Cookie Notice</div>
<!-- OneTrust Cookies List end -->
--}}

		<p><strong>Always read the label.</strong></p>
		<p>Anusol&trade; is formulated for the treatment of haemorrhoids (piles). The Anusol&trade; range includes Anusol&trade; Cream, Anusol&trade; Suppositories, Anusol&trade; Ointment, Anusol&trade; Plus-HC Suppositories, Anusol&trade; Plus-HC Ointment, Anusol&trade; Soothing Relief Suppositories and Anusol&trade; Soothing Relief Ointment, Anusol&trade; Soothing &amp; Cleansing wipes and Anusol&trade; Soothing &amp; Cleansing wipes Convenient Carry Pack.</p>
		<p>This site is published by Church &amp; Dwight UK Limited which is solely responsible for its contents. It is intended for a UK&nbsp;audience.</p>
		<p>All rights reserved. Anusol&trade; is a trademark of Church &amp; Dwight Co., Inc., Ewing, NJ 08628 USA</p>
		<p>Last updated on: 14th January 2021</p>
	</div>
</footer>	
