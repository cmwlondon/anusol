    <div class="clmn our-range">
      <div>
        <div class="frame"><img alt="Our Range" src="/img/icons/our-range.png"></div>
        <h4>Our range</h4>
        <p>The Anusol&trade; range includes creams, ointments, suppositories, and even wipes - everything you&rsquo;ll need for effective relief.</p>
        <a href="/our-range" class="btn blue">Our range</a>
      </div>
    </div>
