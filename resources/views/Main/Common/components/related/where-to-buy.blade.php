  	<div class="clmn where-to-buy">
  		<div>
  			<div class="frame"><img alt="Where to buy" src="/img/icons/where-to-buy.png"></div>
  			<h4>Where to buy?</h4>
  			<p>Anusol™ is available at most supermarkets and pharmacies. Find out where your local shop is, or buy&nbsp;online.</p>
  			<a href="/where-to-buy" class="btn blue">Shop now</a>
  		</div>
  	</div>
