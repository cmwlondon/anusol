  	<div class="clmn get-in-touch">
  		<div>
  			<div class="frame"><img alt="Get in touch" src="/img/icons/get-in-touch.png"></div>
  			<h4>Get in touch</h4>
  			<p>If you&rsquo;d like to get in touch about one of our products, you can talk to us&nbsp;directly.</p>
  			<a href="/get-in-touch" class="btn blue">Contact us</a>
  		</div>
  	</div>
