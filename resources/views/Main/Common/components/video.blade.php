<div class="manualVideo loading" id="{{$id}}" data-mp4="{{$mp4}}" data-webm="{{$webm}}" data-ogv="{{$ogv}}">
	<div class="loader"><div></div></div>

	<div class="video"></div>

	<div class="overlay">
		<div class="rel">
			<div class="frame"><div class="rel"><img alt="" src="{{$poster}}"></div></div>
			<h2>{{$caption}}</h2>
			<button class="play videoTrigger">PLAY</button>
		</div>
	</div>
</div>
