		<header class="page">

      <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="/" title="Anusol&trade;">
          <img alt="Anusol&trade;" src="/img/logo/anusol.png">
        </a>

        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav d-flex justify-content-end">
            <li class="nav-item has-children menu-advice">

              <!-- positioned absolutely within page -->
              <div class="absmenu">
                <div class="dd-wrapper">
                  <div class="dd-item">
                    <div class="fullspan">
                        <div class="thumbnail"><img alt="Our Range" src="/img/common/nav/advice.jpg"></div>
                    </div>
                  </div>
                </div>
              </div>
              <!--  -->

              <div class="relmenu">
                <a href="/piles-advice" class="nav-link @if (($section === 'advice') || (($section === 'advice') && ($page === 'index')) )active @endif">Piles advice</a>

                <!-- positioned absolutely within LI -->
                <div class="dd-wrapper">
                  <div class="dd-item">

                    <div class="dd-menu">
                      
                      <div class="subnav one-column">
                        <div class="clmn">
                          <ul>
                            <li><a href="/piles-advice/causes" class="nav-link @if (($section === 'advice') && ($page === 'causes') ) active @endif">Piles causes</a></li>
                            <li><a href="/piles-advice/symptoms" class="nav-link @if (($section === 'advice') && ($page === 'symptoms') ) active @endif">Piles symptoms</a></li>
                            <li><a href="/piles-advice/treatment" class="nav-link @if (($section === 'advice') && ($page === 'treatment') ) active @endif">Treatment of piles</a></li>
                            <li><a href="/piles-advice/prevention" class="nav-link @if (($section === 'advice') && ($page === 'prevention') ) active @endif">Piles prevention</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <!--  -->

              </div>
            </li>

            <li class="nav-item has-children menu-mums">

              <!-- positioned absolutely within page -->
              <div class="absmenu">
                <div class="dd-wrapper">
                  <div class="dd-item">
                    <div class="fullspan">
                        <div class="thumbnail"><img alt="Our Range" src="/img/common/nav/just-for-mums.jpg"></div>
                    </div>
                  </div>
                </div>
              </div>
              <!--  -->

              <div class="relmenu">
                <a href="/just-for-mums" class="nav-link @if (($section === 'mums') || (($section === 'mums') && ($page === 'index')) )active @endif">Just for mums</a>
                <div class="dd-wrapper">
                  <div class="dd-item">

                    <div class="dd-menu">
                      
                      <div class="subnav one-column">
                        <div class="clmn">
                          <ul>
                            {{-- <li><a href="/just-for-mums/treatment" class="nav-link  @if (($section === 'mums') && ($page === 'treatment') ) active @endif">Treatment of piles</a></li> --}}
                            <li><a href="/just-for-mums/prevention" class="nav-link @if (($section === 'mums') && ($page === 'prevention') ) active @endif">Piles in pregnancy</a></li>
                            <li><a href="/just-for-mums/pregnancy" class="nav-link @if (($section === 'mums') && ($page === 'pregnancy') ) active @endif">Pregnancy resources</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </li>

            <li class="nav-item has-children menu-range">
              <!-- positioned absolutely within page -->
              <!-- 
              <div class="absmenu">
                <div class="dd-wrapper">
                  <div class="dd-item">
                    <div class="fullspan">
                        <div class="thumbnail"><img alt="Our Range" src="/img/common/nav/our-range.jpg"></div>
                    </div>
                  </div>
                </div>
              </div>
            -->
              <!--  -->

              <!-- <div class="relmenu"> -->
                <a href="/our-range" class="nav-link @if (($section === 'our-range') || (($section === 'our-range') && ($page === 'index')) )active @endif">Our range</a>

                <div class="dd-wrapper">
                  <div class="dd-item">

                    <div class="dd-menu">
                      <div class="thumbnail"><img alt="Our Range" src="/img/common/nav/our-range.jpg"></div>
                      <div class="subnav four-column">
                        <div class="clmn cl1">
                          <h4>Cream</h4>
                          <ul>
                            <li><a href="/our-range/cream" class="nav-link @if (($section === 'our-range') && ($page === 'creams') ) active @endif">Cream</a></li>
                          </ul>
                        </div>
                        <div class="clmn cl2">
                          <h4>Ointments</h4>
                          <ul>
                            <li><a href="/our-range/ointment" class="nav-link @if (($section === 'our-range') && ($page === 'ointments') ) active @endif">Ointment</a></li>
                            <li><a href="/our-range/relief-ointment" class="nav-link @if (($section === 'our-range') && ($page === 'ointments-rf') ) active @endif">Soothing Relief Ointment</a></li>
                            <li><a href="/our-range/plus-hc-ointment" class="nav-link @if (($section === 'our-range') && ($page === 'ointments-ph') ) active @endif">Plus-HC Ointment</a></li>
                          </ul>
                        </div>
                        <div class="clmn cl3">
                          <h4>Suppositories</h4>
                          <ul>
                            <li><a href="/our-range/suppositories" class="nav-link @if (($section === 'our-range') && ($page === 'supp') ) active @endif">Suppositories</a></li>
                            <li><a href="/our-range/relief-suppositories" class="nav-link @if (($section === 'our-range') && ($page === 'supp-rf') ) active @endif">Soothing Relief Suppositories</a></li>
                            <li><a href="/our-range/plus-hc-suppositories" class="nav-link @if (($section === 'our-range') && ($page === 'supp-ph') ) active @endif">Plus-HC Suppositories</a></li>
                          </ul>
                        </div>
                        <div class="clmn cl4">
                          <h4>Hygiene wipes</h4>
                          <ul>
                            <li><a href="/our-range/hygiene-wipes" class="nav-link @if (($section === 'our-range') && ($page === 'wipes') ) active @endif">New Soothing &amp; Cleansing wipes</a></li>
                            <li><a href="/our-range/hygiene-wipes" class="nav-link @if (($section === 'our-range') && ($page === 'wipes') ) active @endif">New Convenient Carry&nbsp;Pack</a></li>
                          </ul>
                        </div>
                      </div>                                               
                    </div>

                  </div>
                </div>
              <!-- </div> -->
            </li>

            <li class="nav-item">
              <div class="relmenu">
                <a href="/where-to-buy" class="nav-link @if ( ($section === 'where-to-buy') && ($page === 'index') )active @endif">Where to buy</a>
              </div>
            </li>

            <li class="nav-item has-children menu-faq">
              <!-- positioned absolutely within page -->
              <div class="absmenu">
                <div class="dd-wrapper">
                  <div class="dd-item">
                    <div class="fullspan">
                        <div class="thumbnail"><img alt="Our Range" src="/img/common/nav/faq.jpg"></div>
                    </div>
                  </div>
                </div>
              </div>
              <!--  -->

              <div class="relmenu">
                <a href="/top-questions" class="nav-link  @if (($section === 'faq') || (($section === 'faq') && ($page === 'index')) )active @endif">Top questions</a>
                <div class="dd-wrapper">
                  <div class="dd-item">

                    <div class="dd-menu">
                      <div class="subnav one-column">
                        <div class="clmn">
                          <ul>
                            <li><a href="/top-questions/helpful-links" class="nav-link @if (($section === 'faq') && ($page === 'links') ) active @endif">Helpful links</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </li>

          </ul>        
        </div>
      </nav>

		</header>
