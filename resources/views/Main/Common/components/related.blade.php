{{-- /resources/views/main/common/components/related/ 'where-to-buy','faq','contact','range' --}}
<div class=" @if ( count($links) === 3 ) threeColumnGrid @else fourColumnGrid @endif">
  <div class="row">

    @foreach( $links AS $link)
    @include('Main.Common.components.related/'.$link)
    @endforeach

  </div>
</div>

