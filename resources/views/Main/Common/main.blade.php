<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <title>{!! $meta['title'] !!}</title>
    <meta charset="utf-8">
    <meta name="description" content="{!! $meta['desc'] !!}" >
    <meta name="keywords" content="{!! $meta['keywords'] !!}" >
    <meta name="author" content="{!! $meta['author'] !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="{{ $canonical }}" />
    @foreach ($meta['og'] AS $key => $value)
    	<meta property="og:{{$key}}" content="{{$value}}" />
    @endforeach
    <!-- <link rel="icon" type="/image/png" href="/img/favicon.png"> -->

    <link rel="preload" as="font" href="/fonts/helvetica/font.woff" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/helvetica/font.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/helvetica-bold/font.woff" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/helvetica-bold/font.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/oceansans/font.woff" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/oceansans/font.woff2" type="font/woff2" crossorigin="anonymous">

    <style type="text/css" media="all">
	@import '/css/anusol.css';
	</style>			

    {{-- onetrust code marker --}}
</head>
<body>
	<div class="boundary">
        {{-- @include('Main.Common.components.cookie-consent') --}}
	    @include('Main.Common.components.navigation')
	    <section class="main">
			@yield('content')
		</section>	
		@include('Main.Common.components.footer')
	</div>

    <script>
      var site_url  = "{{ secure_url('',[]) }}";
      @if (isset($pageViewJS))
        var pageViewJS = "{{ $pageViewJS }}";
      @endif
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    {{-- <script src="/js/jquery.lazy.min.js"></script> --}}
    <script src="/js/slick.min.js"></script>
    <script src="/js/anusol.js"></script>
	
    @if ( $extra_js !== '' )
		{!! $extra_js !!}
	@endif
	@include('Main.Common.components.google-analytics')

    <!-- OneTrust Cookies Consent Notice start for anusol.co.uk -->
    <script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="4f76375f-4c1f-4ddd-90b8-f72d9e666fac" ></script>
    <script type="text/javascript">
    function OptanonWrapper() { }
    </script>
    <!-- OneTrust Cookies Consent Notice end for anusol.co.uk -->
</body>
</html>
