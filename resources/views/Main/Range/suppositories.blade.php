@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image">
		<div class="frame"><img alt="Creams" src="/img/range/products/suppositories/product.jpg"></div>
	</div>
	<div class="details">
		<div class="cpy">
			<h1>Anusol&trade; Suppositories</h1>
			<p>Suppositories are inserted directly into the anus, to deliver medication straight to the piles – helping shrink them, soothe itching and relieve pain and discomfort. They&rsquo;re most appropriate for internal piles, and can be used alongside creams and ointments.</p>
			<p>Our suppositories range extends to Anusol&trade; <a href="/our-range/relief-suppositories">Soothing Relief Suppositories</a> with hydrocortisone to help reduce inflammation even faster.</p>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>12 pack</span></li>
				<li><span>24 pack</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li>Wash the anal area, and gently dry with soft towel before using this medicine.</li>
							<li>Remove plastic cover from the suppository before use.</li>
							<li>The suppository will melt after insertion.</li>
							<li>Wash your hands before and after using Anusol&trade;.</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Active substances: Zinc Oxide, Bismuth Oxide, Balsam Peru.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['creams','ointments','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


