@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="bannery hdx loading" id="our-range-banner">
	<div class="image">
		<div class="hdxframe">
			<img alt="" data-d="/img/range/index-page/banner.jpg" data-m="/img/range/index-page/banner-mobile.jpg">
		</div>
	</div>
	<div class="extra">
		<h1>Our Range</h1>
	</div>
</div>

<div class="body bigmargins centred ">
	<p>As uncomfortable and painful as piles may be, they&rsquo;re actually surprisingly common. In fact, 50% of people in the UK will experience them at some point – so there&rsquo;s no need to feel embarrassed!</p>
	<p>In most cases, the symptoms of piles will clear up in around a week or two – with an effective treatment, and a few small changes to your daily&nbsp;routine.</p>

	<p>You can buy a variety of treatments from local pharmacies and major supermarkets. Or, if you&rsquo;d prefer, you can buy them <a href="/where-to-buy">online</a> – they&rsquo;ll arrive within a couple of days in an unmarked&nbsp;package.</p>

	<p>Whether you choose cream, ointment, suppositories or a combination of applications, the Anusol&trade; range provides effective, soothing relief from discomfort and irritation. And, for more complicated piles, the Anusol&trade; Soothing Relief range also offers anti-inflammatory benefits,&nbsp;too.</p>

	<p>While you&rsquo;ve got piles, our Soothing & Cleansing wipes are perfect for cleaning the anal area before applying other Anusol&trade; products, as well as after you&rsquo;ve gone to the&nbsp;loo.</p>	
</div>

<div class="rangeblock" id="range-creams">
	<div class="body centred bigmargins">
		<h2>Cream</h2>
		<p>Piles treatment cream helps shrink less severe piles, reducing painful itching and swelling. Because it has antiseptic properties, it also helps discourage the growth of bacteria&nbsp;cells.</p>		
	</div>
	<div class="items inca switch" data-speed="2500" data-direction="left" data-id="creams" data-mode="switch" data-has-controls="no">
		<div class="row">
			<div class="clmn active">
				<div class="box">
					<a href="/our-range/cream" title="Anusol&trade; Cream" class="fh"><img alt="Anusol&trade; Cream" src="/img/range/index-page/cream.png" class="lazy"></a>
					<h3>Anusol&trade; Cream</h3>
					<a href="/our-range/cream" class="btn blue">View product</a>
				</div>
			</div>
		</div>
		<!-- 
		<div class="index">
			<ul>
				<li class="active">1</li>
			</ul>
		</div>
		-->
	</div>
</div>

<div class="rangeblock" id="range-ointments">
	<div class="body centred bigmargins">
		<h2>Ointments</h2>
		<p>Ointments are very similar to creams, but differ in their consistency and feel. Because they&rsquo;re a tad greasier, you may find they help relieve sore, dry skin,&nbsp;too.</p>		
	</div>
	<div class="items inca switch" data-speed="2500" data-direction="left" data-id="ointments" data-mode="switch" data-has-controls="no">
		<div class="row">
			<div class="clmn active">
				<div class="box">
					<a href="/our-range/ointment" title="Anusol&trade; Ointment" class="fh"><img alt="Anusol&trade; Ointment" src="/img/range/index-page/ointment.png" class="lazy"></a>
					<h3>Anusol&trade; Ointment</h3>
					<a href="/our-range/ointment" class="btn blue">View product</a>
				</div>
			</div>
			<div class="clmn">
				<div class="box">
					<a href="/our-range/relief-ointment" title="Anusol&trade; Soothing Relief Ointment" class="fh"><img alt="Anusol&trade; Soothing Relief Ointment" src="/img/range/index-page/sr-ointment.png" class="lazy"></a>
					<h3>Anusol&trade; Soothing Relief Ointment</h3>
					<a href="/our-range/relief-ointment" class="btn blue">View product</a>
				</div>
			</div>
			<div class="clmn">
				<div class="box">
					<a href="/our-range/plus-hc-ointment" title="Anusol&trade; Plus-HC Ointment" class="fh"><img alt="Anusol&trade; Plus-HC Ointment" src="/img/range/index-page/plus-hc-ointment.png" class="lazy"></a>
					<h3>Anusol&trade; Plus-HC Ointment</h3>
					<a href="/our-range/plus-hc-ointment" class="btn blue">View product</a>
				</div>
			</div>
		</div>

		<div class="index">
			<ul>
				<li class="active">1</li>
				<li>2</li>
				<li>3</li>
			</ul>
		</div>
	</div>
</div>

<div class="rangeblock" id="range-suppositories">
	<div class="body centred bigmargins">
		<h2>Suppositories</h2>
		<p>For internal piles, suppositories can be used alongside creams/ointments. These are inserted directly into the anus, to deliver medication directly to the piles – helping shrink them, soothe itching, and relieve pain and&nbsp;discomfort.</p>		
	</div>
	<div class="items inca switch" data-speed="2500" data-direction="left" data-id="suppositories" data-mode="switch" data-has-controls="no">
		<div class="row">
			<div class="clmn active">
				<div class="box">
					<a href="/our-range/suppositories" title="Anusol&trade; Suppositories" class="fh"><img alt="Anusol&trade; Suppositories" src="/img/range/index-page/suppositories.png" class="lazy"></a>
					<h3>Anusol&trade; Suppositories</h3>
					<a href="/our-range/suppositories" class="btn blue">View product</a>
				</div>
			</div>
			<div class="clmn">
				<div class="box">
					<a href="/our-range/relief-suppositories" title="Anusol&trade; Soothing Relief Suppositories" class="fh"><img alt="Anusol&trade; Soothing Relief Suppositories" src="/img/range/index-page/sr-suppositories.png" class="lazy"></a>
					<h3>Anusol&trade; Soothing Relief Suppositories</h3>
					<a href="/our-range/relief-suppositories" class="btn blue">View product</a>
				</div>
			</div>
			<div class="clmn">
				<div class="box">
					<a href="/our-range/plus-hc-suppositories" title="Anusol&trade; Plus-HC Suppositories" class="fh"><img alt="Anusol&trade; Plus-HC Suppositories" src="/img/range/index-page/plus-hc-suppositories.png" class="lazy"></a>
					<h3>Anusol&trade; Plus-HC Suppositories</h3>
					<a href="/our-range/plus-hc-suppositories" class="btn blue">View product</a>
				</div>
			</div>
		</div>	
		<div class="index">
			<ul>
				<li class="active">1</li>
				<li>2</li>
				<li>3</li>
			</ul>
		</div>
	</div>
</div>

<div class="rangeblock" id="range-wipes">
	<div class="body centred bigmargins">
		<h2>Hygiene wipes</h2>
		<p>When you have piles, wiping with regular toilet paper can feel pretty uncomfortable. For a gentler way to keep your bum both comfortable and clean, we created our New Soothing &amp; Cleansing wipes.</p>		
	</div>
	<div class="items inca switch" data-speed="2500" data-direction="left" data-id="wipes" data-mode="switch" data-has-controls="no">
		<div class="row">
			<div class="clmn active">
				<div class="box">
					<a href="/our-range/hygiene-wipes" title="Anusol&trade; Soothing Wipes" class="fh wide"><img alt="Anusol&trade; Soothing Wipes" src="/img/range/index-page/soothing-wipes.png" class="lazy"></a>
					<h3>Anusol&trade; New Soothing &amp; Cleansing wipes</h3>
					<a href="/our-range/hygiene-wipes" class="btn blue">View product</a>
				</div>
			</div>
			<div class="clmn">
				<div class="box">
					<a href="/our-range/hygiene-wipes" title="Anusol&trade; Convenient Carry Pack" class="fh wide"><img alt="Anusol&trade; Convenient Carry Pack" src="/img/range/index-page/wipes-ccp.png" class="lazy"></a>
					<h3>Anusol&trade; New Convenient Carry Pack</h3>
					<a href="/our-range/hygiene-wipes" class="btn blue">View product</a>
				</div>
			</div>
		</div>	
		<div class="index">
			<ul>
				<li class="active">1</li>
				<li>2</li>
			</ul>
		</div>
	</div>
</div>

<div class="body bigmargins centred">
	<h2>Think you may have piles?</h2>
	<p>Don&rsquo;t keep on guessing. Find out if you have any of the key symptoms.</p>
	<a href="/piles-advice/symptoms" class="btn blue">Symptoms</a>
</div>

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


