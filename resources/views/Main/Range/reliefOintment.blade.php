@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image">
		<div class="frame"><img alt="Creams" src="/img/range/products/sr-ointments/product.jpg"></div>
	</div>

	<div class="details">
		<div class="cpy">
			<h1>Anusol&trade; Soothing Relief Ointment</h1>
			<p>Anusol&trade; Soothing Relief Ointment offers effective relief from internal and external piles, working in four ways by: </p>
			<ul class="blueDiscList">
				<li>shrinking piles</li><li>relieving discomfort</li><li>soothing itching</li><li>reducing inflammation</li>
			</ul>
			<p>Anusol&trade; Soothing Relief Ointment includes hydrocortisone meaning inflammation is reduced, making the Anusol&trade; Soothing Relief range our most complete piles treatment.</p>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>15g</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li><strong>For external piles</strong> – Wash the anal area with gentle soap, and dry it with a soft towel, then gently apply the cream to the affected area.</li><li><strong>For internal piles</strong> – Wash and dry the affected area, screw the nozzle provided onto the tube and remove the nozzle cap. Insert the nozzle into the back passage and squeeze gently. Clean the nozzle after each use.</li><li>Wash your hands before and after using Anusol&trade;.</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Active substances: Zinc Oxide, Bismuth Oxide, Balsam Peru, Hydrocortisone Acetate.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['suppositories','creams','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


