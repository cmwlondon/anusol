@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image pharmacy">
		<div class="frame"><img alt="Creams" src="/img/range/products/ph-suppositories/product.jpg"><div class="phicon"></div></div>
	</div>

	<div class="details">
		<div class="cpy">
			<h1>Anusol™ Plus-HC Suppositories </h1>
			<p>Anusol™ Plus-HC Suppositories contains hydrocortisone which reduces inflammation, and therefore eases the pain and discomfort of haemorrhoids (piles). It also contains active ingredients which soothe the itch and irritation associated with the added&nbsp;haemorrhoids.</p>

			<div class="pharmacy">
				<h2>This is a Pharmacy medicine item</h2>
				<p>You will be asked to complete a short questionnaire which will be checked by a pharmacist to ensure it is suitable for&nbsp;you.</p>
			</div>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>12 pack</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li>Anusol™ Plus-HC Suppositories should be inserted into the anus</li>
							<li>Wash anal area and dry gently with a soft towel before using this medicine</li>
							<li>Remove plastic cover from the suppository before use</li>
							<li>Wash your hands before and after using Anusol™</li>
							<li>The suppository will melt after insertion</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>The active substances per suppositories are: Hydrocortisone acetate, Zinc oxide, Bismuth subgallate, Balsam Peru Bismuth oxide and Benzyl benzoate.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['creams','ointments','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


