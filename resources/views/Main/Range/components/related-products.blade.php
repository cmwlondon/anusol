{{--
#range-creams
#range-ointments
#range-suppositories
#range-wipes
--}}

<div class="items relatedItems">
	<div class="body centred">
		<h2>Our range</h2>
	</div>
	
	<div class="row">

    @foreach( $products AS $product)
    @include('Main.Range.components.products.'.$product)
    @endforeach
		
	</div>	
</div>
