@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image">
		<div class="frame"><img alt="Creams" src="/img/range/products/creams/product.jpg"></div>
	</div>
	<div class="details">
		<div class="cpy">
			<h1>Anusol&trade; Cream</h1>
			<p>Piles treatment creams can help shrink less severe cases of piles, reducing itching and swelling. These products have antiseptic properties, so they also discourage the growth of bacteria cells.</p>
			<p>Anusol&trade; Cream is for topical use only, which means it is applied directly to the affected area. To use, simply rub the cream directly onto the outside of your anus. Anusol&trade; Cream also has lubricating properties for use alongside suppositories.</p>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>23g</span></li>
				<li><span>43g</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li><strong>For external piles</strong> – Wash the anal area with gentle soap, and dry it with a soft towel, then gently apply the cream to the affected area.</li>
							<li><strong>For internal piles</strong> – Wash and dry the affected area, screw the nozzle provided onto the tube and remove the nozzle cap. Insert the nozzle into the back passage and squeeze gently. Clean the nozzle after each use.</li>
							<li>Wash your hands before and after using Anusol&trade;.</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Active substances: Zinc Oxide, Bismuth Oxide, Balsam Peru</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['suppositories','ointments','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


