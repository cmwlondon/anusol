@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image">
		<div class="frame"><img alt="Creams" src="/img/range/products/wipes/product.jpg"></div>
	</div>
 
	<div class="details">
		<div class="cpy">
			<h1>Introducing New Anusol&trade; Soothing &amp; Cleansing wipes</h1>
			<p>When you&rsquo;re suffering from piles, maintaining good anal hygiene is essential, to help reduce itchiness and irritation. Not to mention that, when your bottom&rsquo;s feeling extra-sensitive, loo-roll can feel a little rough!</p>

			<p>Clinically tested, Anusol&trade; Soothing &amp; Cleansing wipes are based on a unique combination of natural ingredients, traditionally known for their soothing, hydrating and cooling properties: witch hazel extract, aloe vera extract, chamomile extract and calendula extract.</p>
			 
			<p>Designed to be used after a bowel movement, instead of regular toilet paper, as well as for cleansing the anal area before applying other Anusol&trade; products: <a href="/our-range/cream">cream</a>, <a href="/our-range#range-ointments">ointments</a> or <a href="/our-range#range-suppositories">suppositories</a>.</p>
			 
			<p>Anusol&trade; Soothing &amp; Cleansing wipes are dermatologically and gynaecologically tested, and can be safely used during pregnancy and breastfeeding.</p>
			 
			<p>New Anusol&trade; Soothing &amp; Cleansing wipes are:</p>
			<ul class="blueDiscList">
				<li>Flushable</li><li>Dermatologically & gynecologically tested</li><li>Hypoallergenic</li><li>Fragrance and alcohol free</li><li>Biodegradable</li>
			</ul>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>30 wipes per pack</span></li>
				<li><span>10 wipes per pack</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li>Use after you&rsquo;ve had a bowel movement, in place of regular toilet tissue.</li><li>Up to two wipes can be flushed at one time.</li><li>Be sure to reseal the pack to stop the wipes drying out.</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Aqua, Hamamelis Virginiana (Witch Hazel) Water, Glycerin, Cocamidopropyl PG-Dimonium Chloride Phosphate, Chamomilla Recutita (Matricaria) Flower Extract, Aloe Barbadensis Leaf Juice, Calendula Officinalis Flower Extract, Sodium Benzoate, Potassium Sorbate, Benzoic Acid, Citric Acid, Sodium Citrate.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['creams','suppositories','ointments']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


