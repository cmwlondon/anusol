@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image pharmacy">
		<div class="frame"><img alt="Creams" src="/img/range/products/ph-ointments/product.jpg"><div class="phicon"></div></div>
	</div>

	<div class="details">
		<div class="cpy">
			<h1>Anusol™ Plus-HC Ointment</h1>
			<p>Anusol™ Plus-HC Ointment offers effective relief from internal and external piles, working in four ways by:</p>
			<ul class="blueDiscList">
				<li>shrinking piles</li><li>relieving discomfort</li><li>soothing itching</li><li>reducing inflammation</li>
			</ul>

			<div class="pharmacy">
				<h2>This is a Pharmacy medicine item</h2>
				<p>You will be asked to complete a short questionnaire which will be checked by a pharmacist to ensure it is suitable for&nbsp;you.</p>
			</div>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>15g</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li><strong>For external piles</strong> – Wash the anal area with gentle soap, and dry it with a soft towel, then gently apply the cream to the affected area.</li><li><strong>For internal piles</strong> – Wash and dry the affected area, screw the nozzle provided onto the tube and remove the nozzle cap. Insert the nozzle into the back passage and squeeze gently. Clean the nozzle after each use.</li><li>Wash your hands before and after using Anusol™.</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Active substances: Hydrocortisone acetate, Zinc oxide, Bismuth subgallate, Balsam Peru, Bismuth oxide and Benzyl benzoate.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['suppositories','creams','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


