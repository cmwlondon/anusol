@extends('Main.Common.main')

@section('header')

@endsection

@section('content')

<div class="product">
	<div class="image">
		<div class="frame"><img alt="Creams" src="/img/range/products/sr-suppositories/product.jpg"></div>
	</div>

	<div class="details">
		<div class="cpy">
			<h1>Anusol&trade; Soothing Relief Suppositories</h1>
			<p>Anusol&trade; Soothing Relief Suppositories offer effective relief from internal piles, working in four ways by:</p>
			<ul class="blueDiscList">
				<li>shrinking piles</li><li>relieving discomfort</li><li>soothing itching</li><li>reducing inflammation</li>
			</ul>
			<p>Designed in a special shape, once inserted into the back passage these suppositories deliver a measured dose of medication, right where you need&nbsp;it.</p>
			<p>Anusol&trade; Soothing Relief includes hydrocortisone meaning inflammation is reduced, making our range our most complete piles treatment.</p>

			<h2>Available sizes:</h2>
			<ul class="sizes">
				<li><span>12 pack</span></li>
			</ul>

			{{-- <a class="download" href="">Download Patient Information Leaflet (PIL)</a> --}}

			<div class="expander">
				<div class="header"><h2>How to use</h2></div>
				<div class="clipper">
					<div class="content">
						<ul class="blueDiscList">
							<li>Wash the anal area, and gently dry with soft towel before using this medicine.</li><li>Remove plastic cover from the suppository before use.</li><li>Insert the suppository into the back passage. It will melt after insertion.</li><li>Wash your hands before and after using Anusol&trade;.</li>
						</ul>
					</div>
				</div>
			</div>

			<p>This product is not recommended for children under the age of&nbsp;18.</p>

			<div class="expander">
				<div class="header"><h2>Ingredients</h2></div>
				<div class="clipper">
					<div class="content">
						<p>Active substances: Zinc Oxide, Bismuth Oxide, Balsam Peru, Hydrocortisone Acetate.</p>
					</div>
				</div>
			</div>

			<div class="buy">
				<a href="/where-to-buy" class="btn blue">Where to buy</a>
			</div>
		</div>
	</div>
</div>

@include('Main.Range.components.related-products',['products' => ['creams','ointments','wipes']])

@include('Main.Common.components.tail')

@endsection

@section('components')
  
@endsection


