function ManualVideo(parameters) {
	this.parameters = parameters;

	this.init();
}

ManualVideo.prototype = {
	"constructor" : ManualVideo,
	"template" : function () {var that = this; },
	
	"init" : function () {
		let that = this;
		// console.log('ManualVideo.init()');

		this.module = $(this.parameters.module);
		this.videoTarget = this.module.find('.video').eq(0);
		this.videoSources = {
			"mp4" : this.module.attr('data-mp4'),
			"webm" : this.module.attr('data-webm'),
			"ogv" : this.module.attr('data-ogv')
		};

		this.delay = this.parameters.loadDelay;

	},

	"prepareVideo" : function () {
		let that = this;
		// console.log('ManualVideo.prepareVideo()');
		this.videoTarget.append( this.insertVideo(this.videoSources) );

		/* manual video 'Play' button */
		$('.videoTrigger').on('click',function(e){
			e.preventDefault();

			console.log('play');
			let module = $(this).parents('.manualVideo').eq(0);
			// remove overlay
			module.addClass('running');

			// start video
			let videoItem = module.find('video').eq(0);
			videoItem.get(0).currentTime = 0;
			videoItem.get(0).play();
		});
	},

	"insertVideo" : function () {
		let that = this;
		// console.log('ManualVideo.insertVideo()');
		this.newVideoTag = $('<video></video>');
		this.newVideoTag
		.attr({
			"preload" : "metadata",
			"controls" : "",
			"playsinline" : "",
			"xloop" : ""
		});

		let types = [["mp4","mp4"],["webm","webm"],["ogv","ogg"]];
		types.map( (type) => this.buildVideoSource(type), this );

		// start video once it is ready to go
		this.newVideoTag.on('loadedmetadata',function(){
			setTimeout( () => that.videoLoaded($(this)), that.delay ); 
		});

		/*
		// loop video
		newVideoTag.on('ended',function(){
			console.log('video ended');
			$(this).get(0).currentTime = 0;
			$(this).get(0).play();
		});
		*/

		return this.newVideoTag;
	},

	"buildVideoSource" : function (source) {
		let sourceID = source[0];
		let sourceType = source[1];
		let videoSource = this.videoSources[ sourceID ];

		if ( videoSource !== '' ) {
			let newVideoSourceItem = $('<source></source>').attr({
				"type" : "video/" + sourceType,
				"src" : videoSource
			});
			this.newVideoTag.append(newVideoSourceItem);
			return sourceID;
		}
	},

	"videoLoaded" : function (videoNode) {
		let that = this;
		// console.log('ManualVideo.videoLoaded()');
		var video = videoNode.get(0),
		readyInfo = video.readyState,
		videoModule = videoNode.parents('.manualVideo').eq(0);
		videoModule.removeClass('loading');
	}
}


export default ManualVideo;