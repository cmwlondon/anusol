function InlineCarousel(parameters) {
	this.parameters = parameters;

	this.id = parameters.id;
	this.slides = parameters.slides;
	this.indicators = parameters.indicators;
	this.controls = parameters.controls;
	this.speed = parameters.speed;
	this.mode = parameters.mode;
	this.hasControls = parameters.hasControls;
	this.breakpoint = parameters.breakpoint;
	this.pagechanged = parameters.pagechanged;

	this.index = 0;
	this.oldIndex;
	this.slideCount = 0;
	this.slideAnimation;
	this.animation_is_running = false;
	this.enabled = false;
	this.installed = false;

	this.init();
}

InlineCarousel.prototype = {
	"constructor" : InlineCarousel,
	"template" : function () {var that = this; },
	
	"init" : function () {
		let that = this;

		this.slideCount = this.slides.length;
		this.module = this.slides.parents('.inca').eq(0);

		this.indexer = this.indicators.parents('.index').eq(0);

		if( this.slideCount > 1)
			this.enabled = true

		if(this.enabled) {
			let windowSize = {
				"w" : $(window).width()
			};

			if( windowSize.w <= this.breakpoint || this.mode !== 'switch' ) {
				this.install();
			}

			if( this.mode === 'switch' ) {
				$(window).smartresize(function(){
				  let windowSize = {
				    "w" : $(window).width()
				  };

				  if( windowSize.w <= that.breakpoint ) {
					that.install();
				  } else {
				  	that.remove();
				  }
				},10);
			}

		}
	},
	"start" : function() {
		let that = this;

		this.slideAnimation = window.setInterval(() => that.transition('next'), this.speed);
		// this.slideAnimation = window.setTimeout(() => that.transition(), this.speed);

		// this.slides.on('transitionend webkitTransitionEnd oTransitionEnd', function (e) {
		// });

		// this.slides.eq(0).get(0).addEventListener('transitionend', function(e){
		// });

		// reset position of slide which has just been moved out of focus			
	},

	"stop" : function() {
		let that = this;

		// stop interval
		clearInterval(this.slideAnimation);
		this.slideAnimation = null;
	},

	"transition" : function(action) {
		let that = this;

		if( this.installed ) {
			let slide = -1;

			if ( this.pagechanged )
				this.pagechanged();

			switch(action) {
				case 'next' : {
					slide = this.nextSlide(this.index);
				} break;
				case 'previous' : {
					slide = this.previousSlide(this.index);
				} break;
			}

			this.transitionToSlide(slide,action);
		}
	},

	"transitionToSlide" : function(slide,action) {
		let that = this;
		let currentSlide = this.slides.eq(this.index);
		let nextSlide = this.slides.eq(slide);
		this.oldIndex = this.index;
		this.index = slide;

		// disable controls while frame transition is running

		this.animation_is_running = true;

		switch(action){
			case 'next' : {
				this.transitionR_L(currentSlide,nextSlide);
			} break;
			case 'previous' : {
				this.transitionL_R(currentSlide,nextSlide);
			} break;
		}


		// update indicators
		this.indicators.eq(this.oldIndex).removeClass('active');
		this.indicators.eq(this.index).addClass('active');
	},

	"transitionR_L" : function(currentSlide,nextSlide) {
		let that = this;

		// transition: new frame slides in from right, current frame slide out on left
		currentSlide
		.css({"opacity":"1","left" : "0%"})
		.animate({"left" : "-100%"}, 600,function(){
			$(this).css({"opacity":"0"});
		});

		nextSlide
		.css({"opacity":"1","left" : "100%"})
		.animate({"left" : "0%"}, 600,function(){
			that.animation_is_running = false;
		});
	},

	"transitionL_R" : function(currentSlide,nextSlide) {
		let that = this;

		// transition: new frame slides in from left, current frame slide out on right
		currentSlide
		.css({"opacity":"1","left" : "0%"})
		.animate({"left" : "100%"}, 600,function(){
			$(this).css({"opacity":"0"});
		});

		nextSlide
		.css({"opacity":"1","left" : "-100%"})
		.animate({"left" : "0%"}, 600,function(){
			that.animation_is_running = false;
		});
	},

	"nextSlide" : function (slide) {
		let nextSlide = slide + 1;

		if ( nextSlide == this.slideCount) {
			nextSlide = 0;
		}

		return nextSlide;
	},

	"previousSlide" : function (slide) {
		let nextSlide = slide - 1;

		if ( nextSlide == -1) {
			nextSlide = (this.slideCount - 1);
		}

		return nextSlide;
	},

	"jumpto" : function (slide) {
		// do nothing if the user clicks on the indicator for the current slide
		if( slide !== this.index) {

			if ( this.pagechanged )
				this.pagechanged();

			if( this.mode === 'switch' || this.mode === 'always' ) {
				// clear interval
				this.stop();
			}

			// do transition
			this.transitionToSlide(slide,'next');

			if( this.mode === 'switch' || this.mode === 'always' ) {
				// start interval
				this.start();
			}
		}
	},

	"docontrol" : function (action) {
		let that = this;

		if( this.mode === 'switch' || this.mode === 'always' ) {
			// clear interval
			this.stop();
		}

		// do transition
		this.transition(action);

		if( this.mode === 'switch' || this.mode === 'always' ) {
			// start interval
			this.start();
		}
	},

	"remove" : function (slide) {
		let that = this;

		if (this.installed) {
			this.stop();

			this.indexer.removeClass('iscontrol');
			this.indicators.off();

			// this.module.animate({"opacity" : 0},100,function(){
				this.installed = false;
				window.setTimeout(() => {
					this.slides.each(function(){
						$(this).css({"left":"0%","opacity":"1"});
					})
					// that.module.animate({"opacity" : 1},100,function(){});
				}, 50);
			// });
		}

	},

	"install" : function (slide) {
		let that = this;

		if (!this.installed) {

			// this.module.animate({"opacity" : 0},100,function(){
				this.installed = false;
				// window.setTimeout(() => {
					this.slides.css({"left":"100%","opacity":"0"});
					this.index = 0;
					this.slides.eq(this.index).css({"left":"0%","opacity":"1"});
					this.installed = true;

					// this.module.animate({"opacity" : 1},100,function(){});

					if( this.hasControls ) {
						// bind events to indicators
						this.indexer.addClass('iscontrol');

						this.indicators.on('click',function(){
							if( !that.animation_is_running ) {
								that.jumpto( that.indicators.index($(this)) );
							}
						});

						// bind events to previous/next controls
						this.controls.on('click',function(){
							if( !that.animation_is_running ) {
								that.docontrol($(this).attr('data-action'));
							}
						});
					}

					if( this.mode === 'switch' || this.mode === 'always' ) {
						this.start();	
					}
					
				// },10)
			// });
		}
	}
}

export default InlineCarousel;
