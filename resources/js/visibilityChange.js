
function getPageVisibilityAPI() {
	let config = {
		"hidden" : null,
		"visibilityChange" : null
	};

	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
	  config.hidden = "hidden";
	  config.visibilityChange = "visibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
	  config.hidden = "msHidden";
	  config.visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
	  config.hidden = "webkitHidden";
	  config.visibilityChange = "webkitvisibilitychange";
	}

	return config;
}

/*
function handleVisibilityChange(){
	
}

document.addEventListener(visibilityChange, handleVisibilityChange, false);
*/