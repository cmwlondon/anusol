function getCookie(n) {
    var t = "(?:; )?" + n + "=([^;]*);?"
      , i = new RegExp(t);
    return i.test(document.cookie) ? decodeURIComponent(RegExp.$1) : null
}

function setCookie(n, t, i, r, u) {
    var f = n + "=" + encodeURIComponent(t);
    i && (f += "; expires=" + i.toGMTString());
    r && (f += "; path=" + r);
    u && (f += "; domain=" + u);
    location.protocol === "https:" && (f += "; secure");
    document.cookie = f
}

$(document).ready(function(){

	/* cookie consent overlay */
	// check cookie consent cookie and display overlay is it is not set
    getCookie("CookieConsent") == null && $(".cookie-notice").addClass('visible');

    // respond to user clicking on 'accept' button
    // close overlay
    // set consent cookie
    $(document).on("click", ".cookie-consent-accept", function(e) {
        $(".cookie-notice").removeClass('visible');
        var n = new Date;
        n.setTime(n.getTime() + 864e8);
        
        // set consent cookie. - DISABLED for DEV/TEST
        setCookie("CookieConsent", "true", n, "/")
    });
});
