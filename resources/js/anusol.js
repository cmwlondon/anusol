// require('./bootstrap');

// require('./cookies'); replaced custom cookie consent banner with onetrust cookie settings banner 14/1/2021
require('./smart-resize');
require('./requestAnimFrame');
// require('./visibilityChange');
// require('./manualvideo');
// require('./InlineCarousel');

import InlineCarousel from './components/InlineCarousel.js'
import ManualVideo from './components/ManualVideo.js'
import QueueSingle from './components/QueueSingle.js'

function toggleExpander( expander ) {
	if( expander.hasClass('open')) {
		expander.removeClass('open');
	} else {
		expander.addClass('open');
	}
}

/*
function getPageVisibilityAPI() {
	let config = {
		"hidden" : null,
		"visibilityChange" : null
	};

	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
	  config.hidden = "hidden";
	  config.visibilityChange = "visibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
	  config.hidden = "msHidden";
	  config.visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
	  config.hidden = "webkitHidden";
	  config.visibilityChange = "webkitvisibilitychange";
	}

	return config;
}

var pageVisibilityAPI;
*/


/* ------------------------------------------------------------------------------------------------ */

$(document).ready(function(){

	// pageVisibilityAPI = getPageVisibilityAPI(); 

	// console.log(pageVisibilityAPI);

	/* ------------------------------------------------------------------------------------- */

	let bannerBreakpoint = 768;

	// https://developer.mozilla.org/en-US/docs/Web/API/MediaQueryList
	// detect when media queries are applied
	// has the benfit of not needing to continuously monitor window width 
	function screenTest(e){
		let _mobileMode = ( e.matches );
		let selector = ( _mobileMode ) ? 'mobile' : 'desktop' ;
		if ( _mobileMode !== mobileMode) {
			// change of state
			let selector = (_mobileMode) ? 'mobile' : 'desktop' ;
			mobileMode = _mobileMode;

			// trigger image switch
			_frames.map(function(_frame) {
				let hdx = _frame.frame.parents('.hdx').eq(0);
				hdx.addClass('loading');
				// _frame._delay = window.setTimeout(function(){
					_frame.frame.find('img').on('load',function(){
						hdx.removeClass('loading');
					}).attr({"src" : _frame[selector]});
				// }, 50);
			});
		}
	}

	// use matchMedia to test if window is wider than 768px or not
	let mql = window.matchMedia('(max-width: 768px)');
	mql.addListener(screenTest);
	let mobileMode = mql.matches;

	let newStateMessage = ( mobileMode ) ? 'mobile' : 'desktop' ;

	let hdx = $('.hdx');
	let frames = $('.hdxframe');

	let _frames = [];

	// init, runs on page load
	// index frames, select appropriate mode for initial image load
	frames.each(function(i){
		
		let frame_img = $(this).find('img');
		let mi = frame_img.attr('data-m');
		let di = frame_img.attr('data-d');

		_frames.push({
			"index" : i,
			"frame" : $(this),
			"mobile" : mi,
			"desktop" : di
		});

		let source = (mobileMode) ? mi : di;
		frame_img
		.on( 'load', function(e) {
			hdx.removeClass('loading');
		} )	
		.attr({ "src" : source });
	});

	/* ------------------------------------------------------------------------------------- */

	/* main navigation menu dropdown */
	/**/
	let menuDelay = null;
	let navMenu = $('.navbar-collapse');
	let navButton = $('.navbar-toggler');

	navButton.on('click',function(e){
		e.preventDefault();

		navButton.toggleClass('collapsed');
		if( navMenu.hasClass('show') ) {
			// close menu
			navMenu
			.animate({"height":"0px"},500,function(e){
				navMenu
				.removeClass('show')
				.css({"height":"","overflow":""}); // remove style tag properties
			});
		} else {
			// open menu
			navMenu
			.css({"height":"0","overflow":"hidden"})
			.addClass('show')
			.animate({"height":"810px"},500,function(e){
				navMenu.css({"height":"","overflow":""}); // remove style tag properties
			});
		}
	});
	/**/

	/*
	$('li.nav-item.has-children').on('hover',function(e){
		$(this).addClass('open-dd');
	});
	*/
	/* ------------------------------------------------------------------------------------- */

	/* manual video loader */
	let videos = [];
	$('.manualVideo').each(function(){
		let subjectVideo = new ManualVideo( {
			"module" : $(this),
			"loadDelay" : 500
		});
		subjectVideo.prepareVideo();
		// setTimeout( () => videoHasLoaded($(this)), 500 ); 
	});

	let carouselVideos = [
		'/video/tv-ad-2kbps.mp4',
		'/video/vid2-2kbps.mp4',
		'/video/vid3-2kbps.mp4',
	]

	/*
	// inline carousel instance unique to home page video carousel
	// allows start of automatic behavour outside of the instance
	let vcItems = null;
	$('#home_vc .inca').each(function(){
		vcItems = new InlineCarousel({
			"id" : $(this).attr('data-id'),
			"slides" : $(this).find('.clmn'),
			"indicators" : $(this).find('.index > ul > li'),
			"controls" : $(this).find('.control'),
			"speed" : $(this).attr('data-speed'),
			"mode" : $(this).attr('data-mode'),
			"hasControls" : ($(this).attr('data-has-controls') == 'yes'),
			"breakpoint" : 920,
			"pagechanged" : function(){
				if ( vcControl.runningVideo !== -1) {
					// reset running video vcControl.runningVideo

					// stop
					vcControl.videoTags[vcControl.runningVideo].pause();

					// show overlay
					vcControl.videoItems[vcControl.runningVideo].removeClass('running');

					vcControl.runningVideo = -1;
				}
			}
		});
	});
	// use this to control videoa across entire video carousel
	// stop running video if frame changes
	let vcControl = {
		"runningVideo" : -1,
		"videoItems" : [],
		"videoTags" : []
	}
	*/

	if($('#home_vc2').length > 0) {
		let vcItems = null;
		let vcControl = {
			"runningVideo" : -1,
			"videoItems" : [],
			"videoTags" : []
		}

		// set up slick slider

        $('#home_vc2 .row').slick({
          "autoplay" : true,
          "autoplaySpeed" : 4000,
          "arrows" : true,
          "dots" : true,
          "swipe" : true,
          "touchMove" : true,
          "slidesToShow" : 3,
          "slidesToScroll" : 3,
          "infinite" : true,
          "responsive" : [
            {
				"breakpoint" : 960,
				"settings" : {
					"slidesToShow" : 2,
					"slidesToScroll" : 2
				}
            }
          ]
        });


		// trigger video overlay when tc advert video item is activated
		$('#home_vc2 .item').each(function(i){
			$(this).find('a').on('click',function(e){
				e.preventDefault();

				let videoItem = $(this).parents('.videoPlay').eq(0);
				let video = videoItem.attr('data-video');
				let caption = videoItem.attr('data-caption');
				let poster = videoItem.attr('data-poster');

				$('#videoCaption').text(caption);

				// insert video tag into popup
				// $('.modal-body')
				// <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video>
				let newVideoTag = $('<video></video>');

				// you can't add the 'muted' attribute after the tag has been created
				newVideoTag
				.attr({
					"preload" : "metadata",
					"controls" : "",
					"playsinline" : "",
					"poster" : poster
				})
				.addClass('waiting');

				// attach mp4 source
				let newsrcMP4 = $('<source></source>').attr({
					"type" : "video/mp4",
					"src" : video
				});
				newVideoTag.append(newsrcMP4);

				$('.modal-body')
				.empty()
				.append(newVideoTag);

				// trigger video lightbox
				$('body')
				.append( $('<div></div>').addClass('modal-backdrop') )
				.addClass('modal-open');
			});
		});
		
        $('#videopopup button.close').on('click', function(e){
            e.preventDefault();

            // stop video

            $('body').removeClass('modal-open');

            $('.modal-backdrop').remove();
            // $('#videopopup').hide();

            $('.modal-body')
            .empty();
        });

        $('.modal-backdrop,.modal').on('click', function(e){
            e.preventDefault();

            // stop video

            $('body').removeClass('modal-open');

            $('.modal-backdrop').remove();
            // $('#videopopup').hide();

            $('.modal-body')
            .empty();
        });

        $(".modal-dialog").click(function(e) {
           // Do something
           e.stopPropagation();
        });
	}


	/* sterimar video pop code

        $('.videoPopupTrigger').on('click', function(e){
          e.preventDefault();

          let videoItem = $(this).parents('.videoPlay').eq(0);
          let video = videoItem.attr('data-video');
          let caption = videoItem.attr('data-caption');
          let poster = videoItem.attr('data-poster');

          // insert video tag into popup
          // $('.modal-body')
          // <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video>
          newVideoTag = $('<video></video>');

          // you can't add the 'muted' attribute after the tag has been created
          newVideoTag
          .attr({
            "preload" : "metadata",
            "controls" : "",
            "playsinline" : "",
            "poster" : poster
          })
          .addClass('waiting');

          // attach mp4 source
          let newsrcMP4 = $('<source></source>').attr({
            "type" : "video/mp4",
            "src" : video
          });
          newVideoTag.append(newsrcMP4);

          $('.modal-body')
          .empty()
          .append(newVideoTag);

          $('#videopopup').show();
          $('#videoCaption').text(caption);
          $('body')
          .append( $('<div></div>').addClass('modal-backdrop') )
          .addClass('modal-open');

          
        });

        $('#videopopup button.close').on('click', function(e){
            e.preventDefault();

            // stop video

            $('body').removeClass('modal-open');

            $('.modal-backdrop').remove();
            $('#videopopup').hide();

            $('.modal-body')
            .empty();
        });

        $('.modal-backdrop,.modal').on('click', function(e){
            e.preventDefault();

            // stop video

            $('body').removeClass('modal-open');

            $('.modal-backdrop').remove();
            $('#videopopup').hide();

            $('.modal-body')
            .empty();
        });

        $(".modal-dialog").click(function(e) {
           // Do something
           e.stopPropagation();
        });

        */

	/* */
	let logoQueue = new QueueSingle({
		"settings" : {
			"delay" : 50 // quarter second delay between each person fades in
		},
		"actions" : {
			"queueStart" : function(queue){},
			"itemStart" : function(queue,worker){

				// queue.settings
				// worker
				// <video preload="metadata" controls="controls" playsinline="" xloop="" data-mp4="/video/tv-ad-2kbps.mp4"><source type="video/mp4" src="/video/tv-ad-2kbps.mp4"></video>

				let newVideoTag = $('<video></vieo>')
				.attr({
					"preload" : "metadata",
					"controls" : "",
					"playsinline" : "",
					"xloop" : ""
				});

				let newVideoSource = $('<source></source>').attr({
					"type" : "video/mp4",
					"src" : worker.source
				});

				newVideoTag.append(newVideoSource);

				let loadTimeout = window.setTimeout(function(){
					console.log("worker load/error timeout %s", worker.index);
					// queue.workerComplete();

					// try again
					worker.target.find('video').remove();

					worker.target.append(newVideoTag);

					newVideoTag.on('loadedmetadata',function(){
						console.log("loaded %s", worker.source);
						clearTimeout(loadTimeout);

						// setTimeout( () => that.videoLoaded($(this)), that.delay ); 
						worker.delay = window.setTimeout(function(){

							vcControl.videoTags[worker.index] =  newVideoTag.get(0);
							queue.workerComplete();	
						}, queue.settings.delay);

					})
				    .on('error', function(e) {
						console.log("error %s", worker.source);
						clearTimeout(loadTimeout);

						worker.delay = window.setTimeout(function(){
							queue.workerComplete();	
						}, queue.settings.delay);
				    });

				}, 500);

				newVideoTag.on('loadedmetadata',function(){
					console.log("loaded %s", worker.source);
					clearTimeout(loadTimeout);

					// setTimeout( () => that.videoLoaded($(this)), that.delay ); 
					worker.delay = window.setTimeout(function(){

						vcControl.videoTags[worker.index] =  newVideoTag.get(0);
						queue.workerComplete();	
					}, queue.settings.delay);

				})
			    .on('error', function(e) {
					console.log("error %s", worker.source);
					clearTimeout(loadTimeout);

					worker.delay = window.setTimeout(function(){
						queue.workerComplete();	
					}, queue.settings.delay);
			    });

			    console.log("appending %s",worker.source);

				worker.target.append(newVideoTag);

				// enable play button
				worker.target.find('.videoTrigger3').on('click',function(e){
					e.preventDefault();

					let thisArticle = $(this).parents('article').eq(0);
					let videoNode = thisArticle.find('video'); 
					let video = videoNode.get(0);
					video.currentTime = 0;
					video.play();

					vcControl.runningVideo = worker.index;
					thisArticle.addClass('running');
				})
			},
			"itemComplete" : function(queue,worker){
			},
			"queueComplete" : function(queue){
				console.log('all videos loaded');
				$('#home_vc').removeClass('loading');
				// vcItems.start();

				var highlightedItems = document.querySelectorAll(".overlay");

				highlightedItems.forEach(function(userItem) {
					// userItem.addEventListener('transitionend', () => { console.log('Transition ended'); });
					// userItem.ontransitionend = () => { console.log('Transition ended'); };

					userItem.addEventListener('transitionend',function(e) {
						console.log('Transition ended');
						console.log(e);
					});

					// e.propertyName 'top'
					// e.srcElement 'div.overlay'
					// e.target 'div.overlay'
				});
				/*
				// *first* instance of .overlay
				const transition = document.querySelector('#home_vc article div.overlay');

				transition.addEventListener('transitionend', () => {
				  console.log('Transition ended');
				});
				*/

			}
		}
	});

	$('#home_vc article').each(function(i){
		let timeInMs = Date.now();
		
		// compile index of video tags
		vcControl.videoItems.push($(this));
		// vcControl.videoTags.push( $(this).find('video').get(0) );
		
		logoQueue.addItem({
			"index" : i,
			"source" : $(this).attr('data-video') + '#' + timeInMs,
			"target" : $(this)
		});
	});

	logoQueue.startQueue();

	/* */

	/* ------------------------------------------------------------------------------------- */

	/* our-range landing page product carousels */
	let incaArray = [];

	$('.inca').not('#home_vc .inca').each(function(){
		incaArray.push(new InlineCarousel({
			"id" : $(this).attr('data-id'),
			"slides" : $(this).find('.clmn'),
			"indicators" : $(this).find('.index > ul > li'),
			"controls" : $(this).find('.control'),
			"speed" : $(this).attr('data-speed'),
			"mode" : $(this).attr('data-mode'),
			"hasControls" : ($(this).attr('data-has-controls') == 'yes'),
			"breakpoint" : 920
		}));
	});

	/* ------------------------------------------------------------------------------------- */
	
	// product page details section expander
	$('.expander .header').on('click',function(e){
		e.preventDefault();
		let expander = $(this).parents('.expander').eq(0);
		toggleExpander( expander );
	})

	/* ------------------------------------------------------------------------------------- */

	// FAQ expander
	$('.expander2 .control').on('click',function(e){
		e.preventDefault();
		let expander = $(this).parents('.expander2').eq(0);
		toggleExpander( expander );
	})

	/* ------------------------------------------------------------------------------------- */

	// FAQ show more/less
	$('.extraQuestion > .header').on('click',function(e){
		e.preventDefault();
		let expander = $(this).parents('.extraQuestion').eq(0);
		toggleExpander( expander );
	})

	/* ------------------------------------------------------------------------------------- */

	// bind google analytics click tracking to 'where to buy' retailer links
	$('.retailGrid .cell').on('click',function(e){
		// e.preventDefault();
		let destination = $(this).attr('href')
		console.log(destination);

		// trigger google analytics click event
		ga('send', 'event', {
			"eventCategory" : 'Outbound Link',
			"eventAction" : 'click',
			"eventLabel" : destination,
			"transport" : 'beacon'
		});
	});

	/* ------------------------------------------------------------------------------------- */

});
